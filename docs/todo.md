### Currently In progress
- Completing version 0.1.0
- Some issues with css borders need to be fixed before I can create a build of the the UI

### Changes needed in API
- UI expects primary roles to be an array
    - Need to update the API to return an array of roles
    - Change from primaryRole to primaryRoles
- Need to make a data endpoint that returns all data
- Ids:
    - Gets should return ids 
    - Delete should be updated to use ID instead of name
    - get for specific resources, if not removed should use ids as well

### Future Features 
- Set up log in
- Set up Router

### Improvements needed for existing features:

#### Adding Items
- Nothing is implemented yet to handle a failed post request (probably need a modal to pop up saying the item could not be added and the placeholder item needs to be removed from the store)
- Need to also have some constraints on what can be done with the placeholder items (for example shouldn't be able to create a new job with the newly added role until it has been confirmed from the server that the role is added) 
    - Possible solution for placeholder items: display loading symbol in list, do not have placeholders appear in other forms, also do not allow updating or removing of item till confirmed

#### Remove Items
- When Item cannot be deleted modal needs to be displayed

### Updating Items
- How to make sure no other user has made update to item right before current user sends his update 
- Employee: should you be able to remove a role if an employee is already assigned to that role for a job

#### Forms
- Validation of input data