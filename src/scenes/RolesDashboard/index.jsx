import React from "react";
import {connect} from "react-redux";
import Dashboard from "../../components/Dashboard";
import EditableRoleList from "./components/EditableRoleList";
import RoleForm from "./components/RoleForm";
import ToggleableForm from "../../components/ToggleableForm";
import {addRole} from "../../services/state-management/data/actions/role/actionCreators";

const RolesDashboard = ({addRole}) => {
    const toggleableForm = (
        <ToggleableForm onFormSubmission={addRole}>
            <RoleForm />
        </ToggleableForm>
    );

    return (
        <Dashboard
            name="Roles"
            itemList={
                <EditableRoleList/>
            }
            toggleableForm={toggleableForm}
        />
    );
};

const mapDispatchToProps = {
    addRole: addRole
};

export default connect(
    null,
    mapDispatchToProps
)(RolesDashboard);