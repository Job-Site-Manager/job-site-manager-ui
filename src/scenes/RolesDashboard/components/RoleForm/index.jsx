import React from 'react';
import InputFormEntry from "../../../../components/InputFormEntry";
import TextAreaFormEntry from "../../../../components/TextAreaFormEntry";
import Form from "../../../../components/Form";
import { useFormInputs } from "../../../../services/custom-hooks/state/index";

const RoleForm = ({role, nextId, onFormSubmission, onCancelClicked, submissionButtonName}) => {
    const name = useFormInputs(role.name);
    const notes = useFormInputs(role.notes);

    const handleFormSubmission = () => {
        onFormSubmission(
            {
                id: role.id,
                name: name.value,
                notes: notes.value
            }
        );
    };

    return (
        <Form
            submissionButtonName={submissionButtonName}
            onCancelClicked={onCancelClicked}
            onFormSubmission={handleFormSubmission}
        >
            <div>
                <InputFormEntry
                    name="Name"
                    value={name.value}
                    onInputChange={name.onChange}
                />
                <TextAreaFormEntry
                    name="Notes"
                    value={notes.value}
                    onTextAreaChange={notes.onChange}
                />
            </div>
        </Form>
    );
};

RoleForm.defaultProps = {
    role: {},
};

export default RoleForm;