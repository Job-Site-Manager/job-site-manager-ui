import React from "react";
import { connect } from "react-redux";
import styled from 'styled-components';
import EditableItem from "../../../../components/EditableItem";
import RoleForm from "../RoleForm";
import reducer from "../../../../services/state-management/data/reducers";
import {deleteRole, updateRole} from "../../../../services/state-management/data/actions/role/actionCreators";

const RoleBodyWrapper = styled.div`
    text-align: center;
`;

const RoleListWrapper = styled.ul`
    margin: 0;  
    padding: 0;
`;

const EditableRoleList = ({onRoleUpdate, onRemoveRole, roles}) => {
    const editableRoleComponents = roles.map((role) => {
        let notes = role.notes ? role.notes : "No notes available.";

        const itemBody = (
            <RoleBodyWrapper>
              {notes}
            </RoleBodyWrapper>
        );

        return (
            <EditableItem
                key={role.id}
                name={role.name}
                id={role.id}
                itemBody={itemBody}
                onUpdate={onRoleUpdate}
                onRemoveItem={onRemoveRole}
                form={
                    <RoleForm
                        submissionButtonName="Update"
                        id={role.id}
                        role={role}
                    />
                }
            />
        )
    });

    return (
        <RoleListWrapper>
            {editableRoleComponents}
        </RoleListWrapper>
    );
};

const mapStateToProps = (state) => {
    return {
        roles: reducer.roles.getAllRoles(state),
    };
};

const mapDispatchToProps = {
    onRoleUpdate: updateRole,
    onRemoveRole: deleteRole
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditableRoleList);
