import React from "react";
import styled from 'styled-components';
import JobDetails from "../JobDetails";
import EditableItem from "../../../../components/EditableItem";
import JobForm from "../JobForm";
import {connect} from "react-redux";
import reducer from "../../../../services/state-management/data/reducers";
import {deleteJob, updateJob} from "../../../../services/state-management/data/actions/job/actionCreators";

const EditableJobListWrapper = styled.ul`
    margin: 0;  
    padding: 0;
`;

const EditableJobList = ({jobs, onJobUpdate, onRemoveJob}) => {
    const editableJobComponents = jobs.map((job) => (
        <EditableItem
            key={job.id}
            name={job.name}
            id={job.id}
            itemBody={<JobDetails job={job}/>}
            onUpdate={onJobUpdate}
            onRemoveItem={onRemoveJob}
            form={<JobForm job={job} submissionButtonName="Update"/>}
        />
    ));

    return (
        <EditableJobListWrapper>
            {editableJobComponents}
        </EditableJobListWrapper>
    )
};

const mapStateToProps = (state) => {
    return {
        jobs: reducer.jobs.getAllJobs(state)
    };
};

const mapDispatchToProps = {
    onJobUpdate: updateJob,
    onRemoveJob: deleteJob
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditableJobList);