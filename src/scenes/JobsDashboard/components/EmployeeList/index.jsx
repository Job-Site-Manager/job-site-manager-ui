import React from "react";
import styled from 'styled-components';
import ItemDetail from "../../../../components/ItemDetail/index";
import uuid from "uuid";

const List = styled.ul`
    margin: 0;
    padding: 0;
`;

const EmployeeItem = styled.div`
    border-bottom-style: solid;
    border-bottom-width: 1px;
    text-align: center;
    font-size: 14px;
    padding: 2px;
    
    :nth-last-child(1) {
        border-bottom: none;
    }
`;

const EmployeeList = ({employees, idealNumberOfEmployees}) => {
    const employeeList = employees.map((employee) => (
        <EmployeeItem key={employee.name}>{employee.name}</EmployeeItem>
    ));

    while(employeeList.length < idealNumberOfEmployees) {
        employeeList.push(
            <EmployeeItem key={uuid.v4()}>(Available Position)</EmployeeItem>
        );
    }

    return (
        <ItemDetail name="Assigned Employees">
            <List>{employeeList}</List>
        </ItemDetail>
    );
};

export default EmployeeList;