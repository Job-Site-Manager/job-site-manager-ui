import React from "react";
import ItemDetail from "../../../../../../components/ItemDetail/index";
import EmployeeList from "../../../EmployeeList/index";
import styled from "styled-components";

const JobPositionWrapper = styled.section`
    margin-bottom: 1px;
`;

const Title = styled.h4`
    margin: 0 8px 8px;
    padding: 2px;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    font-size: 14px;
    text-align: center;
`;

const JobPosition = ({jobPosition}) => (
    <JobPositionWrapper>
        <Title>{jobPosition.role.name}</Title>
        <ItemDetail
            name="Required Number of Employees"
            value={jobPosition.idealNumberOfEmployees}
        />
        <ItemDetail
            name="Number of Spots Left to Fill"
            value={jobPosition.idealNumberOfEmployees - jobPosition.employees.length}
        />
        <EmployeeList
            employees={jobPosition.employees}
            idealNumberOfEmployees={jobPosition.idealNumberOfEmployees}
        />
    </JobPositionWrapper>
);


export default JobPosition;