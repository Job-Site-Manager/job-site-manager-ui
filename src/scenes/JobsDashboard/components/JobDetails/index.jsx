import React from "react";
import uuid from 'uuid';
import ItemDetail from "../../../../components/ItemDetail/index";
import ItemContentSection from "../../../../components/ItemContentSection/index";
import JobPosition from "./components/JobPosition/index";
import NameList from "../../../../components/NameList";

const JobDetails = ({job}) => {
    const jobPositionsComponents = job.jobPositions.map(
        jobPosition => <JobPosition key={uuid.v4()} jobPosition={jobPosition} />
    );
    const roleNames = job.jobPositions.map(jobPosition => jobPosition.role.name);

    return (
        <section>
            <ItemContentSection title="Job Details" centerTitle>
                <ItemDetail name="Address" value={job.address}/>
            </ItemContentSection>
            <ItemContentSection title="Required Roles" centerTitle>
                <NameList names={roleNames}/>
            </ItemContentSection>
            <ItemContentSection title="Job Positions" centerTitle>
                {jobPositionsComponents}
            </ItemContentSection>
        </section>
    )
};

export default JobDetails;