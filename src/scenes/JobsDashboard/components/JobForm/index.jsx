import React, {useState} from 'react';
import InputFormEntry from "../../../../components/InputFormEntry";
import SelectItemsDashboard from "../../../../components/SelectItemsDashboard";
import ItemContentSection from "../../../../components/ItemContentSection";
import JobPositionInputForm from "./components/JobPositionFormInput";
import Form from "../../../../components/Form";
import uuid from "uuid";
import {useFormInputs} from "../../../../services/custom-hooks/state";
import reducer from "../../../../services/state-management/data/reducers";
import {connect} from "react-redux";

const getTheAssignedRoles = jobPositions => jobPositions.map(jobPosition => jobPosition.role);

const filterItemsOutOfListById = (list, idsToRemove) => list.filter(item => !idsToRemove.includes(item.id));

const getAssignedEmployees = (jobPositions) => {
    let assignedEmployees = [];

    jobPositions.forEach((jobPosition) => {
        assignedEmployees = assignedEmployees.concat(jobPosition.employees);
    });

    return assignedEmployees;
};

const JobForm = ({
    job,
    allRoles,
    allEmployees,
    onFormSubmission,
    submissionButtonName,
    onCancelClicked
}) => {
    const name = useFormInputs(job.name);
    const address = useFormInputs(job.address);
    const [jobPositions, setJobPositions] = useState(job.jobPositions || []);
    const assignedRoles = getTheAssignedRoles(jobPositions);
    const availableRoles = filterItemsOutOfListById(allRoles, assignedRoles.map(role => role.id));
    const availableEmployees = filterItemsOutOfListById(
        allEmployees, getAssignedEmployees(jobPositions).map(employee => employee.id)
    );

    const handleJobPositionIdealNumberChange = (event, roleName) => {
        const updatedJobPositions = jobPositions.map((jobPosition) => {
            if(jobPosition.role.name === roleName) {
                return Object.assign({}, jobPosition, {idealNumberOfEmployees: parseInt(event.target.value)});
            } else {
                return jobPosition;
            }
        });

        setJobPositions(updatedJobPositions);
    };

    const addJobPosition = role => setJobPositions(jobPositions.concat({role, employees: [], id: uuid.v4()}));

    const removeJobPosition = role =>
        setJobPositions(jobPositions.filter(jobPosition => jobPosition.role.id !== role.id));

    const addEmployeeToJobPosition = (jobPositionId, employee) => {
        const updatedJobPositions = jobPositions.map((jobPosition) => {
            if(jobPosition.id === jobPositionId) {
                const employees = jobPosition.employees.concat(employee);
                return Object.assign({}, jobPosition, {employees: employees});
            } else {
                return jobPosition;
            }
        });

        setJobPositions(updatedJobPositions);
    };

    const removeEmployeeFromJobPosition =  (jobPositionId, employeeId) => {
        const updatedJobPositions = jobPositions.map((jobPosition) => {
            if(jobPosition.id === jobPositionId) {
                    const employees = jobPosition.employees.filter(employee => employee.id !== employeeId);
                    return Object.assign({}, jobPosition, {employees: employees});
            }
            else {
                return jobPosition;
            }
        });

        setJobPositions(updatedJobPositions);
    };

    const handleFormSubmission = () => onFormSubmission(
        {
            id: job.id,
            name: name.value,
            address: address.value,
            jobPositions: jobPositions
        }
    );


    const jobPositionInputForms = jobPositions.map(jobPosition => (
        <JobPositionInputForm
            key={uuid.v4()}
            jobPosition={jobPosition}
            availableEmployees={availableEmployees}
            addEmployeeToJobPosition={addEmployeeToJobPosition}
            removeEmployeeFromJobPosition={removeEmployeeFromJobPosition}
            onIdealNumberOfEmployeesChange={handleJobPositionIdealNumberChange}
        />
    ));

    return (
        <Form
            submissionButtonName={submissionButtonName}
            onCancelClicked={onCancelClicked}
            onFormSubmission={handleFormSubmission}
        >
            <div>
                <InputFormEntry name="Name" value={name.value} onInputChange={name.onChange}/>
                <InputFormEntry name="Address" value={address.value} onInputChange={address.onChange}/>
                <SelectItemsDashboard
                    title={"Required Roles"}
                    selectedItems={assignedRoles}
                    availableItems={availableRoles}
                    addItem={addJobPosition}
                    removeItem={removeJobPosition}
                />
                <ItemContentSection title="Job Positions">
                    {jobPositionInputForms}
                </ItemContentSection>
            </ div>
        </Form>
    );
};

JobForm.defaultProps = {
    job: {
        jobPositions: [],
    },
    assignedRoles: [],
    allRoles: [],
    allEmployees: []
};

const mapStateToProps = (state, {job, onFormSubmission, submissionButtonName, onCancelClicked}) => {
    const allEmployees =  reducer.employees.getAllEmployees(state);
    const allRoles =  reducer.roles.getAllRoles(state);

    return {
        job,
        allRoles,
        allEmployees: allEmployees,
        onFormSubmission,
        submissionButtonName,
        onCancelClicked,
    };
};

export default connect(
    mapStateToProps,
)(JobForm);