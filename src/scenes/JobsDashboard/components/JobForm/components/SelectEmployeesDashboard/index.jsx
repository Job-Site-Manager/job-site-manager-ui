import React from 'react';
import styled from "styled-components";
import SelectItemsDashboard from "../../../../../../components/SelectItemsDashboard";
import ItemDetail from "../../../../../../components/ItemDetail";

// TODO: clean up this styling, it's not ideal
const SelectEmployeesDashboardWrapper = styled(SelectItemsDashboard)`
    margin-bottom: 6px;
    > ul {
        margin-left: -2px;
        margin-right: -2px;
            
        > div {
            margin: 0;
            border-style: none;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            box-shadow: none;
        }
    }
    
    > div {
        margin-top: 10px;
    }
    
    > button {
        margin-top: 10px;
    }
`;

const SelectEmployeesDashboard = ({
    selectedEmployees,
    availableEmployees,
    idealNumberOfEmployees,
    addItem,
    removeItem
}) => {
    // TODO: When the idealNumberOfEmployees is null the adding employees should be inaccessible
    // TODO: When the idealNumberOfEmployees is set to a smaller number a warning modal should come up to
    // remove employees
    const employeesDashboard = (
        <SelectEmployeesDashboardWrapper
            maxNumberOfItems={idealNumberOfEmployees}
            selectedItems={selectedEmployees}
            availableItems={availableEmployees}
            addItem={addItem}
            removeItem={removeItem}
        />
    );

    return (
        <div>
            <ItemDetail name="Assigned Employees" value={employeesDashboard}/>
        </div>
    )
};

export default SelectEmployeesDashboard;