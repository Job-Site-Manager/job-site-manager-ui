import React from 'react';
import styled from "styled-components";
import InputFormEntry from "../../../../../../components/InputFormEntry";
import SelectEmployeesDashboard from "../SelectEmployeesDashboard";

const JobPositionWrapper = styled.section`
    margin-bottom: 1px;
`;

const Title = styled.h4`
    margin: 0 0 8px;
    padding: 2px;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    font-size: 14px;
`;

const JobPositionFormInput = ({
    jobPosition,
    availableEmployees,
    idealNumberOfEmployees,
    addEmployeeToJobPosition,
    removeEmployeeFromJobPosition,
    onIdealNumberOfEmployeesChange,
}) => {
    const addEmployee = (employee) => addEmployeeToJobPosition(jobPosition.id, employee);

    const removeEmployee = (employee) => removeEmployeeFromJobPosition(jobPosition.id, employee.id);

    const handleIdealNumberOfEmployeesChange = event => onIdealNumberOfEmployeesChange(event, jobPosition.role.name);

    return (
        <JobPositionWrapper>
            <Title>{jobPosition.role.name}</Title>
            <InputFormEntry
                name="Number of required employees"
                value={jobPosition.idealNumberOfEmployees}
                onInputChange={handleIdealNumberOfEmployeesChange}
            />
            <SelectEmployeesDashboard
                selectedEmployees={jobPosition.employees}
                availableEmployees={availableEmployees}
                idealNumberOfEmployees={jobPosition.idealNumberOfEmployees}
                addEmployee={addEmployeeToJobPosition}
                addItem={addEmployee}
                removeItem={removeEmployee}
            />
        </JobPositionWrapper>
    )
};

export default JobPositionFormInput;