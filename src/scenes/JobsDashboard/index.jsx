import React from 'react';
import Dashboard from '../../components/Dashboard';
import EditableJobList from './components/EditableJobList';
import JobForm from './components/JobForm';
import ToggleableForm from "../../components/ToggleableForm";
import {addJob} from "../../services/state-management/data/actions/job/actionCreators";
import {connect} from "react-redux";

const JobsDashboard = ({addJob}) => {
    const toggleableForm = (
        <ToggleableForm onFormSubmission={addJob}>
            <JobForm/>
        </ToggleableForm>
    );

    return (
        <Dashboard
            name="Jobs"
            itemList={<EditableJobList />}
            toggleableForm={toggleableForm}
        />
    );
};

const mapDispatchToProps = {
    addJob: addJob
};

export default connect(
    null,
    mapDispatchToProps
)(JobsDashboard);

