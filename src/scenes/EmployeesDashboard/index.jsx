import React from "react";
import Dashboard from "../../components/Dashboard";
import EmployeeForm from './components/EmployeeForm';
import EditableEmployeeList from "./components/EditableEmployeeList";
import ToggleableForm from "../../components/ToggleableForm";
import {connect} from "react-redux";
import {addEmployee} from "../../services/state-management/data/actions/employee/actionCreators";

const EmployeesDashboard = ({addEmployee}) => {
    const toggleableForm = (
        <ToggleableForm onFormSubmission={addEmployee}>
            <EmployeeForm/>
        </ToggleableForm>
    );

    return (
        <Dashboard
            name="Employees"
            itemList={
                <EditableEmployeeList />
            }
            toggleableForm={toggleableForm}
        />
    );
};

const mapDispatchToProps = {
    addEmployee: addEmployee
};

export default connect(
    null,
    mapDispatchToProps
)(EmployeesDashboard);
