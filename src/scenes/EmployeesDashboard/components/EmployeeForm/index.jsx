import React from 'react';
import InputFormEntry from "../../../../components/InputFormEntry";
import SelectItemsDashboard from "../../../../components/SelectItemsDashboard";
import Form from "../../../../components/Form";
import {useFormInputs, useObjectArrayState} from "../../../../services/custom-hooks/state/index";
import {connect} from "react-redux";
import reducer from "../../../../services/state-management/data/reducers";

const filterItemsOutOfListById = (list, idsToRemove) => list.filter(item => !idsToRemove.includes(item.id));

const useRolesState = (initialState) => {
    const objectsArray = useObjectArrayState(initialState);

    const addRole = role => objectsArray.addObject(role);

    const removeRole = role => objectsArray.removeObject(role.id);

    return {
        value: objectsArray.value,
        addRole,
        removeRole
    }
};

const EmployeeForm = ({
    employee,
    allRoles,
    onFormSubmission,
    submissionButtonName,
    onCancelClicked
}) => {
    const name = useFormInputs(employee.name);
    const address = useFormInputs(employee.address);
    const phoneNumber = useFormInputs(employee.phoneNumber);
    const primaryRoles = useRolesState(employee.primaryRoles);
    const secondaryRoles = useRolesState(employee.secondaryRoles);

    const getAvailableRoles = () => filterItemsOutOfListById(
        allRoles,
        primaryRoles.value.map(role => role.id).concat(secondaryRoles.value.map(role => role.id))
    );

    const handleFormSubmission = () => {
        onFormSubmission(
            {
                id: employee.id,
                name: name.value,
                address: address.value,
                phoneNumber: phoneNumber.value,
                primaryRoles: primaryRoles.value,
                secondaryRoles: secondaryRoles.value,
            }
        );
    };

    return (
        <Form
            submissionButtonName={submissionButtonName}
            onCancelClicked={onCancelClicked}
            onFormSubmission={handleFormSubmission}
        >
            <div>
                <InputFormEntry
                    name="Name"
                    value={name.value}
                    onInputChange={name.onChange}
                />
                <InputFormEntry
                    name="Address"
                    value={address.value}
                    onInputChange={address.onChange}
                />
                <InputFormEntry
                    name="Phone Number"
                    value={phoneNumber.value}
                    onInputChange={phoneNumber.onChange}
                />
                <SelectItemsDashboard
                    title={"Primary Roles"}
                    selectedItems={primaryRoles.value}
                    availableItems={getAvailableRoles()}
                    addItem={primaryRoles.addRole}
                    removeItem={primaryRoles.removeRole}
                />
                <SelectItemsDashboard
                    title={"Secondary Roles"}
                    selectedItems={secondaryRoles.value}
                    availableItems={getAvailableRoles()}
                    addItem={secondaryRoles.addRole}
                    removeItem={secondaryRoles.removeRole}
                />
            </ div>
        </Form>
    );
};

EmployeeForm.defaultProps = {
    employee: {
        primaryRoles: [],
        secondaryRoles: []
    }
};

const mapStateToProps = (
    state,
    {
        employee,
        onFormSubmission,
        submissionButtonName,
        onCancelClicked
    }
) => {
    const allRoles = reducer.roles.getAllRoles(state);

    return {
        employee,
        allRoles,
        onFormSubmission,
        submissionButtonName,
        onCancelClicked,
    };
};

export default connect(
    mapStateToProps,
)(EmployeeForm);