import React from "react";
import ItemDetail from "../../../../components/ItemDetail/index";
import ItemContentSection from "../../../../components/ItemContentSection/index";
import NameList from "../../../../components/NameList/index";

const EmployeeDetails = ({employee}) => (
    <section>
        <ItemContentSection title="Employee Details" centerTitle>
            <ItemDetail name="Address" value={employee.address}/>
            <ItemDetail name="Phone Number" value={employee.phoneNumber}/>
        </ItemContentSection>
        <ItemContentSection title="Primary Roles" centerTitle>
            <NameList names={employee.primaryRoles.map(role => role.name)}/>
        </ItemContentSection>
        <ItemContentSection title="SecondaryRoles" centerTitle>
            <NameList names={employee.secondaryRoles.map(role => role.name)}/>
        </ItemContentSection>
    </section>
);

export default EmployeeDetails;