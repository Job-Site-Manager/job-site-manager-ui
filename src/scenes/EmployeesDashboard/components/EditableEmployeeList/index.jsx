import React from "react";
import styled from 'styled-components';
import EmployeeDetails from "../EmployeeDetails";
import EditableItem from "../../../../components/EditableItem";
import EmployeeForm from "../EmployeeForm";
import reducer from "../../../../services/state-management/data/reducers";
import {connect} from "react-redux";
import {updateEmployee, deleteEmployee} from "../../../../services/state-management/data/actions/employee/actionCreators";

const EmployeeListWrapper = styled.ul`
    margin: 0;  
    padding: 0;
`;

const EditableEmployeeList = ({employees, onEmployeeUpdate, onRemoveEmployee}) => {
    const editableRoleComponents = employees.map((employee) => (
        <EditableItem
            key={employee.id}
            id={employee.id}
            name={employee.name}
            itemBody={<EmployeeDetails employee={employee}/>}
            onUpdate={onEmployeeUpdate}
            onRemoveItem={onRemoveEmployee}
            form={
                <EmployeeForm
                    employee={employee}
                    submissionButtonName="Update"
                />
            }
        />
    ));

    return (
        <EmployeeListWrapper>
            {editableRoleComponents}
        </EmployeeListWrapper>
    );
};

const mapStateToProps = (state) => {
    return {
        employees: reducer.employees.getAllEmployees(state)
    };
};

const mapDispatchToProps = {
    onEmployeeUpdate: updateEmployee,
    onRemoveEmployee: deleteEmployee
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditableEmployeeList);