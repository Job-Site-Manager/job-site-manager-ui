export const baseUrl = "http://localhost:8080";

const checkStatus = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        throw error;
    }
};

const checkForCreatedStatus = (response) => {
    if (response.status === 201) {
        return response;
    } else {
        const error = new Error(`HTTP Error ${response.statusText}`);
        error.status = response.statusText;
        error.response = response;
        throw error;
    }
};

const handleError = (error, onError) => onError();

const parseJSON = response => response.json();

export const getData = (url, onSuccess, onError) => {
    return fetch(url, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => checkStatus(response))
        .then(parseJSON)
        .then(onSuccess)
        .catch(error => handleError(error, onError));
};

export const getAllData = (success, error) => getData(baseUrl + "/data", success, error);

const postData = (data, endpoint, onSuccess, onError) => {
    return fetch(baseUrl + endpoint, {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then((response) => checkForCreatedStatus(response))
        .then(parseJSON)
        .then(onSuccess)
        .catch(error => handleError(error, onError));
};

export const postRole = (role, success, error) => postData(role, "/role", success, error);

export const postEmployee = (employee, success, error) => postData(employee, "/employee", success, error);

export const postJob = (job, success, error) => postData(job, "/job", success, error);

const deleteData = (endpoint, onError) => {
    return fetch(baseUrl + endpoint, {
        method: 'delete',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then(checkStatus)
        .catch(error => handleError(error, onError));
};

export const deleteRole = (id, onError) => deleteData('/role/' + id, onError);

export const deleteEmployee = (id, onError) => deleteData('/employee/' + id, onError);

export const deleteJob = (id, onError) => deleteData('/job/' + id, onError);

const updateData = (body, endpoint, onError) => {
    return fetch(baseUrl + endpoint, {
        method: 'put',
        body: JSON.stringify(body),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then(checkStatus)
        .catch(error => handleError(error, onError));
};

export const updateRole = (role, onError) => updateData(role, `/role/${role.id}`, onError);

export const updateEmployee = (employee, onError) => updateData(employee, `/employee/${employee.id}`, onError);

export const updateJob = (job, onError) => updateData(job, `/job/${job.id}`, onError);