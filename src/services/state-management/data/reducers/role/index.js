import * as roleActions from '../../actions/role/actions';
import {checkForInvalidIds, getIds, removeObjectFromState} from "../utils/index";

const addRoleToState = (state, role) => ({...state, [role.id]: role});

const roles = (state = {}, action) => {
    if(roleActions.isTypeResetRoles(action)) {
        return action.roles;
    }
    else if(roleActions.isTypeAddPlaceholderRole(action) ||
        roleActions.isTypeUpdateRole(action) ||
        roleActions.isTypeDeleteRoleFailed(action) ||
        roleActions.isTypeUpdateRoleFailed(action)
    ) {
        return addRoleToState(state, action.role);
    }
    else if(roleActions.isTypeAddRole(action)) {
        return addRoleToState(
            removeObjectFromState(state, action.placeholderId),
            action.role
        );
    }
    else if (roleActions.isTypeDeleteRole(action)) {
        const newState = {...state};
        delete newState[action.id];
        return newState;
    } else {
        return state;
    }
};

const getRoles = (state, roleIds) => {
    checkForInvalidIds(state, roleIds, "roles");

    return roleIds.map(roleId => state[roleId]);
};

const getRole = (state, roleId) => getRoles(state, [roleId])[0];

const getAllRoles = state => getRoles(state, getIds(state));

export default {
    reducer: roles,
    selectors: {
        getRole,
        getRoles,
        getAllRoles
    }
}