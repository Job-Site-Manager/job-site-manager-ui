import roleReducer from './index';
import * as roleActionCreators from '../../actions/role/actionCreators';
import {verifyReducer} from "../testUtils/index";

describe('Role Reducer', () => {
    const state = {
        1: {
            id: 1,
            name: "Bulldozer Operator",
            notes: "Requires a bulldozer"
        },
        2: {
            id: 2,
            name: "Backhoe Operator",
            notes: "Requires a backhoe"
        },
        3: {
            id: 3,
            name: "Rock Truck Operator",
            notes: null
        },
        4: {
            id: 4,
            name: "Scraper Operator",
            notes: "Driver should not have a sore back"
        },
        5: {
            id: 5,
            name: "Packer Operator",
            notes: "Requires a packer"
        }
    };

    describe('Selector', () => {
        describe('getAllRoles', () => {
            it('Returns all roles', () => {
                const expectedRoleArray = [
                    state[1],
                    state[2],
                    state[3],
                    state[4],
                    state[5]
                ];

                expect(roleReducer.selectors.getAllRoles(state)).toEqual(expectedRoleArray);
            });

            it('If no roles exist it returns an empty array', () => {
                expect(roleReducer.selectors.getAllRoles({})).toEqual([]);
            });
        });

        describe('getRole', () => {
            it('For valid role id the matching role is returned', () => {
                expect(roleReducer.selectors.getRole(state, 4)).toEqual(state[4],);
            });

            it('If the role id does not match any roles an error is thrown', () => {
                expect(() => roleReducer.selectors.getRole(state, 99)).toThrow();
            });
        });

        describe('getRoles', () => {
            it('For valid role ids the matching roles are returned', () => {
                const expectedRoleArray = [
                    state[1],
                    state[4],
                    state[5]
                ];

                expect(roleReducer.selectors.getRoles(state, [1,4,5])).toEqual(expectedRoleArray);
            });

            it('If the role ids do not match any roles an error is thrown', () => {
                expect(() => roleReducer.selectors.getRoles(state, [99,100])).toThrow();
            });
        });
    });

    describe('Reducer', () => {
        describe('Add Placeholder Role Action', () => {
            it('Adds the correct role', () => {
                const role = {
                    id: 6,
                    name: 'Labourer',
                    notes: 'Needs to be in good shape',
                };

                const resultState = {
                    ...state,
                    [role.id]: {
                        ...role,
                        placeholder: true
                    }
                };

                verifyReducer({
                    state,
                    action: roleActionCreators.addPlaceholderRole(role),
                    resultState,
                    reducer: roleReducer.reducer
                });
            });
        });

        describe('Delete Role Action', () => {
            it('Deletes the correct role', () => {
                const id = 1;

                const resultState = {...state, [id]: undefined};

                verifyReducer({
                    state,
                    action: roleActionCreators.createDeleteRoleAction(id),
                    resultState,
                    reducer: roleReducer.reducer
                });
            });
        });

        describe('Add Role Action', () => {
            it('Updates the correct role', () => {
                const role = {
                    id: 1,
                    name: "Bulldozer Operator",
                    notes: "Requires that the bulldozer operator is qualified"
                };

                const resultState = {...state, [role.id]: role};

                verifyReducer({
                    state,
                    action: roleActionCreators.createUpdateRoleAction(role),
                    resultState,
                    reducer: roleReducer.reducer
                });
            });
        });
    });

});