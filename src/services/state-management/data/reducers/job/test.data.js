export const state = {
    jobs:{
        1: {
            id: 1,
            name: "The Old Pond",
            address: "23 Pond Rd.",
            jobPositions: [
                1,
                2
            ]
        },
        2: {
            id: 2,
            name: "Gravel Pit",
            address: "434 Beach Rd.",
            jobPositions: [
                3
            ]
        }
    },
    jobPositions: {
        1: {
            id: 1,
            role: 3,
            idealNumberOfEmployees: 2,
            employees: [
                5,
                4
            ]
        },
        2: {
            id: 2,
            role: 2,
            idealNumberOfEmployees: 1,
            employees: [
                2
            ]
        },
        3: {
            id: 3,
            role: 1,
            idealNumberOfEmployees: 2,
            employees: [
                1
            ]
        }
    }
};