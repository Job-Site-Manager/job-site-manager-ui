import {checkForInvalidIds, getIds} from "../utils/index";

export const getJobPositions = (state, jobPositionIds) => {
    checkForInvalidIds(state.jobPositions, jobPositionIds, "job positions");

    return jobPositionIds.map(jobPositionId => state.jobPositions[jobPositionId]);
};

export const getJob = (state, jobId) => {
    const job = state.jobs[jobId];
    const jobPositions = getJobPositions(state, job.jobPositions);

    return {
        ...job,
        jobPositions: jobPositions
    }
};

export const getAllJobIds = state => getIds(state.jobs);

export const getAllJobs = state => getIds(state.jobs).map(jobId => state.jobs[jobId]);

export const getEmployeesFromJobPositions = (state, jobPositionIds) => {
    let employees = [];

    if(!jobPositionIds) {
        return employees;
    }

    jobPositionIds.forEach((jobPositionId) => {
        employees = employees.concat(state.jobPositions[jobPositionId].employees);
    });

    return employees;
};

export const getAllEmployeeIdsForAJob = (state, jobId) => {
    if(!state.jobs[jobId]) {
        throw new Error(`No job exists with the id: ${jobId}`);
    }

    return getEmployeesFromJobPositions(state, state.jobs[jobId].jobPositions);
};

export const getRolesFromJobPositions = (state, jobPositionIds) => {
    const roles = [];

    if(!jobPositionIds) {
        return roles;
    }

    jobPositionIds.forEach((jobPositionId) => {
        roles.push(state.jobPositions[jobPositionId].role);
    });

    return roles;
};

export const getAllRoleIdsForAJob = (state, jobId) => {
    if(!state.jobs[jobId]) {
        throw new Error(`No job exists with the id: ${jobId}`);
    }

    return getRolesFromJobPositions(state, state.jobs[jobId].jobPositions);
};

const isEmployeeAssignedToJobPosition = (jobPosition, employeeId) => jobPosition.employees.includes(employeeId);

const isEmployeeAssignedToJob = (state, job, employeeId) => {
    const jobPositions = getJobPositions(state, job.jobPositions);

    for(let i = 0; i <jobPositions.length; i++) {
        if(isEmployeeAssignedToJobPosition(jobPositions[i], employeeId)) {
            return true;
        }
    }

    return false;
};


export const getJobsEmployeeIsAssignedTo = (state, employeeId) => {
    const jobs = [];

    getAllJobs(state).forEach(job => {
        if(isEmployeeAssignedToJob(state, job, employeeId)) {
            jobs.push(job);
        }
    });

    return jobs;
};

export const isEmployeeAssignedToAtLeastOneJob = (state, employeeId) => {
    return getJobsEmployeeIsAssignedTo(state, employeeId).length >= 1;
};

const isRoleAssignedToJobPosition = (jobPosition, roleId) => jobPosition.role === roleId;

const isRoleAssignedToJob = (state, job, roleId) => {
    const jobPositions = getJobPositions(state, job.jobPositions);

    for(let i = 0; i <jobPositions.length; i++) {
        if(isRoleAssignedToJobPosition(jobPositions[i], roleId)) {
            return true;
        }
    }

    return false;
};

export const getJobsRoleIsAssignedTo = (state, roleId) => {
    const jobs = [];

    getAllJobs(state).forEach(job => {
        if(isRoleAssignedToJob(state, job, roleId)) {
            jobs.push(job);
        }
    });

    return jobs;
};

export const isRoleAssignedToAtLeastOneJob = (state, roleId) => {
    return getJobsRoleIsAssignedTo(state, roleId).length >= 1;
};

