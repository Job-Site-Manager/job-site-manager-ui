import {verifyReducer} from "../testUtils/index";
import jobReducer from './reducer';
import * as jobActionCreators from "../../actions/job/actionCreators";
import {state} from './test.data'

describe('Job Reducer', () => {
    describe('Add Placeholder Job Action', () => {
        it('Adds the correct job', () => {
            const newJob = {
                id: 3,
                name: "The New Job",
                address: "1 Pond Rd.",
                jobPositions: [
                    {
                        id: 4,
                        role: {id: 4},
                        idealNumberOfEmployees: 2,
                        employees: [
                            {id: 5,}, {id: 4,}
                        ],
                    },
                    {
                        id: 5,
                        role: {id: 3},
                        idealNumberOfEmployees: 2,
                        employees: [
                            {id: 6}, {id: 7}
                        ],
                    },
                ],
            };

            const resultJobs = {
                ...state.jobs,
                [newJob.id]: {
                    ...newJob,
                    jobPositions: [
                        newJob.jobPositions[0].id,
                        newJob.jobPositions[1].id
                    ],
                    placeholder: true,
                }
            };

            const resultJobPositions = {
                ...state.jobPositions,
                [newJob.jobPositions[0].id]: {
                    ...newJob.jobPositions[0],
                    role: 4,
                    employees: [5, 4],
                    placeholder: true,
                },
                [newJob.jobPositions[1].id]: {
                    ...newJob.jobPositions[1],
                    role: 3,
                    employees: [6, 7],
                    placeholder: true,
                },
            };

            const resultState = {
                jobs: resultJobs,
                jobPositions: resultJobPositions
            };

            verifyReducer({
                state,
                action: jobActionCreators.addPlaceholderJob(newJob),
                resultState,
                reducer: jobReducer
            });
        });
    });

    describe('Delete Job Action', () => {
        it('Deletes the correct job', () => {
            const id = 1;
            const jobPositionIds = state.jobs[id].jobPositions;

            const resultState = {
                jobs: {
                    ...state.jobs,
                    [id]: undefined
                },
                jobPositions: {
                    ...state.jobPositions,
                    [jobPositionIds[0]]: undefined,
                    [jobPositionIds[1]]: undefined,
                }
            };

            verifyReducer({
                state,
                action: jobActionCreators.createDeleteJobAction(id),
                resultState,
                reducer: jobReducer
            });
        });
    });

    describe('Update Job Action', () => {
        it('Updates the correct job', () => {
            const updatedJob = {
                ...state.jobs[1],
                address: "1 New Rd.",
                jobPositions: [
                    {
                        ...state.jobPositions[1],
                        role: {id: 3},
                        employees: [
                            {id: 6}, {id: 7}, {id: 8}
                        ]
                    },
                    {
                        ...state.jobPositions[2],
                        role: {id: 1},
                        employees: [
                            {id: 2}, {id: 4}, {id: 5}
                        ]
                    },
                    {
                        id: 4,
                        role: {id: 2},
                        idealNumberOfEmployees: 1,
                        employees: [
                            {id: 3}
                        ]
                    }
                ]
            };

            const resultJobs = {
                ...state.jobs,
                [updatedJob.id]: {
                    ...updatedJob,
                    jobPositions: [
                        updatedJob.jobPositions[0].id,
                        updatedJob.jobPositions[1].id,
                        updatedJob.jobPositions[2].id
                    ]
                }
            };

            const resultJobPositions = {
                ...state.jobPositions,
                [updatedJob.jobPositions[0].id]: {
                    ...updatedJob.jobPositions[0],
                    role: 3,
                    employees: [6, 7, 8]
                },
                [updatedJob.jobPositions[1].id]: {
                    ...updatedJob.jobPositions[1],
                    role: 1,
                    employees: [2, 4, 5]
                },
                [updatedJob.jobPositions[2].id]: {
                    ...updatedJob.jobPositions[2],
                    role: 2,
                    employees: [3]
                }
            };

            const resultState = {
                jobs: resultJobs,
                jobPositions: resultJobPositions
            };

            verifyReducer({
                state,
                action: jobActionCreators.createUpdateJobAction(updatedJob),
                resultState,
                reducer: jobReducer
            });
        });

    });
});
