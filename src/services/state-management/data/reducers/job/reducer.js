import * as jobActions from "../../actions/job/actions";

const defaultState = {
    jobs: {},
    jobPositions: {}
};

const createNewJobPositions = (job) => {
    let newJobPositions = {};

    job.jobPositions.forEach(jobPosition => {
        const employeeIds = jobPosition.employees.map(employee => employee.id);
        newJobPositions = {
            ...newJobPositions,
            [jobPosition.id]: {
                ...jobPosition,
                role: jobPosition.role.id,
                employees: employeeIds
            }
        };
    });

    return newJobPositions;
};

const addJobAndJobPositionsToState = (state, job) => {
    const jobPositionsIds = job.jobPositions.map(jobPosition => jobPosition.id);

    const newJob = {
        ...job, jobPositions: jobPositionsIds
    };

    const newJobPositions = createNewJobPositions(job);

    return {
        jobs: {...state.jobs, [newJob.id]: newJob},
        jobPositions: {...state.jobPositions, ...newJobPositions}
    };
};

const removeJobFromState = (state, id) => {
    const updatedJobPositions = {...state.jobPositions};
    state.jobs[id].jobPositions.forEach(jobPositionId => delete updatedJobPositions[jobPositionId]);

    const updateJobs = {...state.jobs};
    delete updateJobs[id];

    return {
        jobs: updateJobs,
        jobPositions: updatedJobPositions
    }
};

const reducer = (state = defaultState, action) => {
    if(jobActions.isTypeResetJobs(action)) {
        return action.jobs;
    } else if(
        jobActions.isTypeAddPlaceholderJob(action)||
        jobActions.isTypeUpdateJob(action) ||
        jobActions.isTypeDeleteJobFailed(action) ||
        jobActions.isTypeUpdateJobFailed(action)
    ) {
        return addJobAndJobPositionsToState(state, action.job);
    } else if (jobActions.isTypeDeleteJob(action)) {
        return removeJobFromState(state, action.id)
    } else if(jobActions.isTypeAddJob(action)) {
        const stateWithoutPlaceholderJob = removeJobFromState(state, action.placeholderId);
        return addJobAndJobPositionsToState(stateWithoutPlaceholderJob, action.job);
    } else {
        return state;
    }
};

export default reducer;