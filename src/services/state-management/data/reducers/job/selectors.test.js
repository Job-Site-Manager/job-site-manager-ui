import {createCopy} from "../testUtils/index";
import * as selectors from './selectors';
import {state} from './test.data';

describe('Job Selectors', () => {
    describe('getJob', () => {
        it('For valid job id the matching job is returned with job positions', () => {
            const expectedJob = {
                id: 1,
                name: "The Old Pond",
                address: "23 Pond Rd.",
                jobPositions: [
                    {
                        id: 1,
                        role: 3,
                        idealNumberOfEmployees: 2,
                        employees: [
                            5,
                            4
                        ]
                    },
                    {
                        id: 2,
                        role: 2,
                        idealNumberOfEmployees: 1,
                        employees: [
                            2
                        ]
                    }
                ]
            };

            expect(selectors.getJob(state, 1)).toEqual(expectedJob);
        });

        it('Does not modify the state', () => {
            const initialState = createCopy(state);
            selectors.getJob(state, 1);
            expect(state).toEqual(initialState);
        });

    });

    describe('getAllJobs', () => {
        it('Returns all jobs', () => {
            const expectedJobs = [
                state.jobs[1], state.jobs[2]
            ];

            expect(selectors.getAllJobs(state)).toEqual(expectedJobs);
        });

        it('If no jobs exist returns an empty array', () => {
            expect(selectors.getAllJobs({jobs: {}})).toEqual([]);
        });
    });

    describe('getJobPositions', () => {
        it('For valid job position ids the matching job positions are returned', () => {
            const expectedJobPositions = [
                state.jobPositions[1], state.jobPositions[2]
            ];

            expect(selectors.getJobPositions(state, [1, 2])).toEqual(expectedJobPositions);
        });

        it('If the job position ids do not match any job positions an error is thrown', () => {
            expect(() => selectors.getJobPositions(state, [99, 100])).toThrow();
        });
    });

    describe('getAllEmployeesForAJob', () => {
        it('The employee ids from all job positions for a job are returned', () => {
            const expectedEmployeeIds = [5, 4, 2];

            expect(selectors.getAllEmployeeIdsForAJob(state, 1)).toEqual(expectedEmployeeIds);
        });

        it('If the job does not exist an error should be throw', () => {
            expect(() => selectors.getAllEmployeeIdsForAJob(state, 99)).toThrow();
        });
    });

    describe('getAllRoleIdsForAJob', () => {
        it('The Role ids from all job positions for a job are returned', () => {
            const expectedEmployeeIds = [3, 2];

            expect(selectors.getAllRoleIdsForAJob(state, 1)).toEqual(expectedEmployeeIds);
        });

        it('If the job does not exist an error should be throw', () => {
            expect(() => selectors.getAllRoleIdsForAJob(state, 99)).toThrow();
        });
    });

    describe('getJobsEmployeeIsAssignedTo', () => {
        it('Should return the jobs the employee is assigned to', () => {
            const expectedJobs = [
                state.jobs[2]
            ];

            expect(selectors.getJobsEmployeeIsAssignedTo(state, 1)).toEqual(expectedJobs);
        });

        it('Should return no jobs the because employee is not assigned to any', () => {
            expect(selectors.getJobsEmployeeIsAssignedTo(state, 7)).toEqual([]);
        });
    });

    describe('isEmployeeAssignedToAtLeastOneJob', () => {
        it('Should return true because employee is assigned to a job', () => {
            expect(selectors.isEmployeeAssignedToAtLeastOneJob(state, 1)).toBeTruthy();
        });

        it('Should return no jobs the because employee is not assigned to any', () => {
            expect(selectors.isEmployeeAssignedToAtLeastOneJob(state, 7)).toBeFalsy();
        });
    });

    describe('getJobsRoleIsAssignedTo', () => {
        it('Should return the jobs the role is assigned to', () => {
            const expectedJobs = [
                state.jobs[1]
            ];

            expect(selectors.getJobsRoleIsAssignedTo(state, 3)).toEqual(expectedJobs);
        });

        it('Should return no jobs the because role is not assigned to any', () => {
            expect(selectors.getJobsRoleIsAssignedTo(state, 7)).toEqual([]);
        });
    });

    describe('isRoleAssignedToAtLeastOneJob', () => {
        it('Should return true because role is assigned to a job', () => {
            expect(selectors.isRoleAssignedToAtLeastOneJob(state, 3)).toBeTruthy();
        });

        it('Should return no jobs the because role is not assigned to any', () => {
            expect(selectors.isRoleAssignedToAtLeastOneJob(state, 7)).toBeFalsy();
        });
    });
});