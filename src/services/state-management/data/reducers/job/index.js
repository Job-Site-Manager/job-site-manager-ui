import * as selectors from './selectors';
import reducer from './reducer';

export default {
    selectors,
    reducer
}