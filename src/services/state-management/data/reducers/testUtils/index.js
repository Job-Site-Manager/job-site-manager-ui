export const createCopy = object => JSON.parse(JSON.stringify(object));

export const verifyReducer = ({state, action, resultState, reducer}) => {
    const stateCopy = createCopy(state);
    const newState = reducer(state, action);

    expect(newState).toEqual(resultState);
    expect(state).toEqual(stateCopy);
};