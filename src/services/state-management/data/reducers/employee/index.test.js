import {verifyReducer} from "../testUtils/index";
import * as employeeActionCreator from "../../actions/employee/actionCreators";
import employeeReducer from "./index";

describe('Employee Reducer', () => {
    const state = {
        1: {
            id: 1,
            name: "Tom Houston",
            address: "12 Steep Road",
            phoneNumber: "519-425-6434",
            primaryRoles: [
                1
            ],
            secondaryRoles: [
                2,
                4
            ]
        },
        2: {
            id: 2,
            name: "Bill Crosby",
            address: "23 Rocky Road",
            phoneNumber: "519-734-9921",
            primaryRoles: [
                2
            ],
            secondaryRoles: [
                3,
                4,
                5
            ]
        },
        3: {
            id: 3,
            name: "Wayne Gretzky",
            address: "99 Greztky Lane",
            phoneNumber: "519-528-1884",
            primaryRoles: [
                4
            ],
            secondaryRoles: []
        }
    };

    describe('Selectors', () => {
        describe('getAllEmployeeIds', () => {
            it('Returns all employee ids', () => {
                const expectedEmployees = ["1", "2", "3"];

                expect(employeeReducer.selectors.getAllEmployeeIds(state)).toEqual(expectedEmployees);
            });

            it('If no employees exist returns an empty array', () => {
                expect(employeeReducer.selectors.getAllEmployeeIds({})).toEqual([]);
            });
        });

        describe('getAllEmployees', () => {
            it('Should return all employees', () => {
                expect(employeeReducer.selectors.getAllEmployees(state)).toEqual(
                    [state[1], state[2], state[3]]
                );
            });
        });

        describe('getEmployees', () => {
            it('For valid employee ids the matching employees are returned', () => {
                const expectedEmployees = [state[2], state[3]];

                expect(employeeReducer.selectors.getEmployees(state, [2, 3])).toEqual(expectedEmployees);
            });

            it('If the employee ids do not match any employees an error is thrown', () => {
                expect(() => employeeReducer.selectors.getEmployees(state, [99, 100])).toThrow();
            });
        });

        describe('getEmployeesRoleIsAssignedTo', () => {
            it('Should return the employees the role is assigned to', () => {
                const expectedEmployees = [
                    state[1], state[2]
                ];

                expect(employeeReducer.selectors.getEmployeesRoleIsAssignedTo(state, 2)).toEqual(expectedEmployees);
            });

            it('Should return no employees because the role is not assigned to any', () => {
                expect(employeeReducer.selectors.getEmployeesRoleIsAssignedTo(state, 7)).toEqual([]);
            });
        });

        describe('isRoleAssignedToAtLeastOneEmployee', () => {
            it('Should return true because employee is assigned to a job', () => {
                expect(employeeReducer.selectors.isRoleAssignedToAtLeastOneEmployee(state, 2)).toBeTruthy();
            });

            it('Should return no jobs the because employee is not assigned to any', () => {
                expect(employeeReducer.selectors.isRoleAssignedToAtLeastOneEmployee(state, 7)).toBeFalsy();
            });
        });
    });

    describe('Reducer', () => {
        describe('Add Placeholder Employee Action', () => {
            it('Adds the correct placeholder employee', () => {
                const employee = {
                    id: 4,
                    name: "John Doe",
                    address: "23 Rocky Road",
                    phoneNumber: "519-734-9921",
                    primaryRoles: [
                        {id: 2}
                    ],
                    secondaryRoles: [
                        {id: 3},
                        {id: 4},
                        {id: 5}
                    ],
                };

                const resultState = {
                    ...state,
                    [employee.id]: {
                        ...employee,

                        primaryRoles: [2],
                        secondaryRoles: [3, 4, 5],
                        placeholder: true,
                    }
                };

                verifyReducer({
                    state,
                    action: employeeActionCreator.addPlaceholderEmployee(employee),
                    resultState,
                    reducer: employeeReducer.reducer
                });
            });
        });

        describe('Delete Employee Action', () => {
            it('Deletes the correct employee', () => {
                const id = 1;

                const resultState = {...state, [id]: undefined};

                verifyReducer({
                    state,
                    action: employeeActionCreator.createDeleteEmployeeAction(id),
                    resultState,
                    reducer: employeeReducer.reducer
                });
            });
        });

        describe('Update Employee Action', () => {
            it('Updates the correct employee', () => {
                const employee = {
                    id: 1,
                    name: "Tom Houston",
                    address: "31 New Street",
                    phoneNumber: "519-425-6434",
                    primaryRoles: [
                        {id: 3}
                    ],
                    secondaryRoles: [
                        {id: 2},
                        {id: 4},
                        {id: 5}
                    ]
                };

                const resultState = {
                    ...state,
                    [employee.id]: {
                        ...employee,
                        primaryRoles: [3],
                        secondaryRoles: [2, 4, 5]
                    }
                };

                verifyReducer({
                    state,
                    action: employeeActionCreator.createUpdateEmployeeAction(employee),
                    resultState,
                    reducer: employeeReducer.reducer
                });
            });
        });
    });
});