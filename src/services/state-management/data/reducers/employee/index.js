import * as employeeActions from "../../actions/employee/actions";
import {checkForInvalidIds, getIds, removeObjectFromState} from "../utils/index";

const addEmployeeToState = (state, employee) => ({
    ...state,
    [employee.id]: {
        ...employee,
        primaryRoles: employee.primaryRoles.map(role => role.id),
        secondaryRoles: employee.secondaryRoles.map(role => role.id),
    }
});

const employees = (state = {}, action) => {
    if(employeeActions.isTypeResetEmployees(action)) {
        return action.employees;
    }
    else if(
        employeeActions.isTypeUpdateEmployee(action) ||
        employeeActions.isTypeAddPlaceholderEmployee(action) ||
        employeeActions.isTypeDeleteEmployeeFailed(action) ||
        employeeActions.isTypeUpdateEmployeeFailed(action)
    ) {
        return addEmployeeToState(state, action.employee);
    }
    else if(employeeActions.isTypeAddEmployee(action)) {
        return addEmployeeToState(
            removeObjectFromState(state, action.placeholderId),
            action.employee
        );
    }
    else if(employeeActions.isTypeDeleteEmployee(action)) {
        const newState = {...state};
        delete newState[action.id];
        return newState;
    }
    else {
        return state;
    }
};

const getEmployees = (state, employeeIds) => {
    checkForInvalidIds(state, employeeIds, "employees");
    return employeeIds.map(employeeId => state[employeeId]);
};

const getAllEmployeeIds = state => getIds(state);

const getAllEmployees = (state) => getEmployees(state, getIds(state));

const isRoleAssignedToEmployee = (state, employee, roleId) => {
    return employee.primaryRoles.includes(roleId) || (employee.secondaryRoles.includes(roleId));
};

export const getEmployeesRoleIsAssignedTo = (state, roleId) => {
    const employees = [];

    getAllEmployees(state).forEach(employee => {
        if(isRoleAssignedToEmployee(state, employee, roleId)) {
            employees.push(employee);
        }
    });

    return employees;
};

export const isRoleAssignedToAtLeastOneEmployee = (state, roleId) => {
    return getEmployeesRoleIsAssignedTo(state, roleId).length >= 1;
};

export default {
    reducer: employees,
    selectors: {
        getEmployees,
        getAllEmployees,
        getAllEmployeeIds,
        getEmployeesRoleIsAssignedTo,
        isRoleAssignedToAtLeastOneEmployee
    }
}