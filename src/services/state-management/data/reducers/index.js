import { combineReducers } from 'redux';
import roleReducer from './role/index';
import employeeReducer from './employee/index';
import jobReducer from './job/index';

const reducer = combineReducers({
    roles: roleReducer.reducer,
    employees: employeeReducer.reducer,
    jobs: jobReducer.reducer
});

const getData = state => {
    return state.data;
};

const getRole = (state, roleId) => roleReducer.selectors.getRole(getData(state).roles, roleId);

const getRoles = (state, roleIds) => roleReducer.selectors.getRoles(getData(state).roles, roleIds);

const getAllRoles = state => roleReducer.selectors.getAllRoles(getData(state).roles);

const getEmployees = (state, employeeIds) => {
    const employees = employeeReducer.selectors.getEmployees(getData(state).employees, employeeIds);

    return employees.map(employee => ({
        ...employee,
        primaryRoles: getRoles(state, employee.primaryRoles),
        secondaryRoles: getRoles(state, employee.secondaryRoles)
    }));
};

const getEmployee = (state, id) => getEmployees(state, [id])[0];

const getAllEmployeeIds = state => employeeReducer.selectors.getAllEmployeeIds(getData(state).employees);

const getAllEmployees = state => getEmployees(state, getAllEmployeeIds(state));

const getAllJobIds = state => jobReducer.selectors.getAllJobIds(getData(state).jobs);

const getJob = (state, jobId) => {
    const job = jobReducer.selectors.getJob(getData(state).jobs, jobId);

    const jobPositions = job.jobPositions.map(jobPosition => {
        const employees = getEmployees(state, jobPosition.employees);
        const role = getRole(state, jobPosition.role);

        return {
            ...jobPosition,
            role,
            employees
        }
    });

    return {
        ...job,
        jobPositions
    };
};

const getAllJobs = state => {
    const jobIds = getAllJobIds(state);

    return jobIds.map(jobId => getJob(state, jobId));
};

const isEmployeeAssignedToAtLeastOneJob = (state, employeeId) => {
    return jobReducer.selectors.isEmployeeAssignedToAtLeastOneJob(getData(state).jobs, employeeId);
};

const getJobsEmployeeIsAssignedTo = (state, employeeId) => {
    return jobReducer.selectors.getJobsEmployeeIsAssignedTo(getData(state).jobs, employeeId);
};

const isRoleAssignedToAtLeastOneJob = (state, employeeId) => {
    return jobReducer.selectors.isRoleAssignedToAtLeastOneJob(getData(state).jobs, employeeId);
};

const getJobsRoleIsAssignedTo = (state, employeeId) => {
    return jobReducer.selectors.getJobsRoleIsAssignedTo(getData(state).jobs, employeeId);
};

const isRoleAssignedToAtLeastOneEmployee = (state, employeeId) => {
    return employeeReducer.selectors.isRoleAssignedToAtLeastOneEmployee(getData(state).employees, employeeId);
};

const getEmployeesRoleIsAssignedTo = (state, employeeId) => {
    return employeeReducer.selectors.getEmployeesRoleIsAssignedTo(getData(state).employees, employeeId);
};

export default {
    reducer,
    roles: {
        getRole,
        getRoles,
        getAllRoles
    },
    employees: {
        getEmployees,
        getEmployee,
        getAllEmployees,
        isRoleAssignedToAtLeastOneEmployee,
        getEmployeesRoleIsAssignedTo
    },
    jobs: {
        getAllJobs,
        getJob,
        isEmployeeAssignedToAtLeastOneJob,
        getJobsEmployeeIsAssignedTo,
        isRoleAssignedToAtLeastOneJob,
        getJobsRoleIsAssignedTo
    }
}