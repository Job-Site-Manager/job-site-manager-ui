import {getNextFreeId, removeItemsFromArray} from "./index";

describe('Reducer utils', () => {
    describe('getNextFreeId', () => {
        it('Returns the next free id', () => {
            const object = { 1: 'a', 2: 'b', 3: 'c' };
            expect(getNextFreeId(object)).toEqual(4);
        });
    });

    describe('removeItemsFromArray', () => {
        it('Returns an array with the correct items removed', () => {
            const array = [1,2,3,4,5,6,7];
            const itemsToRemove = [3,5,7];

            expect(removeItemsFromArray(array, itemsToRemove)).toEqual([1,2,4,6]);
        });
    });
});