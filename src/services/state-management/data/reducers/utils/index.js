export const getIds = object => Object.keys(object);

export const getNextFreeId = object => {
    const ids = getIds(object);

    if(ids.length === 0) {
        return 1;
    } else {
        return Math.max(...ids) + 1;
    }
};

export const removeItemsFromArray = (allItems, itemsToRemove) =>
    allItems.filter(item => !itemsToRemove.includes(item));

const findInvalidIds = (object, ids) => {
    const invalidIds = [];
    ids.forEach(id => {
        if(!object[id]) {
            invalidIds.push(id);
        }
    });

    return invalidIds;
};

export const checkForInvalidIds = (object, ids, objectType) => {
    const invalidIds = findInvalidIds(object, ids);

    if(invalidIds.length !== 0) {
        throw new Error(`No ${objectType} exist for the following ids: ${invalidIds.join(',')}`)
    }
};

export const removeObjectFromState = (state, id) => {
    const stateWithFieldRemoved = {...state};
    delete stateWithFieldRemoved[id];

    return stateWithFieldRemoved;
};