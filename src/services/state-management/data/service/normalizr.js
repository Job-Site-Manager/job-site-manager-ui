import { normalize, schema } from 'normalizr';
import jobs from './test-data/jobs';
import employees from './test-data/employees';
import roles from './test-data/roles';

const roleSchema = new schema.Entity('roles');

const employeeSchema = new schema.Entity('employees', {
    primaryRoles: [roleSchema],
    secondaryRoles: [roleSchema]
});

const jobPositionSchema = new schema.Entity('jobPositions', {
    role: roleSchema,
    employees: [employeeSchema]
});

const jobSchema = new schema.Entity('jobs', {
    jobPositions: [jobPositionSchema]
});

export const dataSchema = new schema.Entity('data', {
    roles: [roleSchema],
    employees: [employeeSchema],
    jobs: [jobSchema]
});

const normalizeData = ({roles, employees, jobs}) => normalize({roles, employees, jobs}, dataSchema).entities;

export const normalizeRoleList = (roles) => normalizeData({roles}).roles;

export const normalizeEmployeeList = (employees) => normalizeData({employees}).employees;

export const normalizeJobList = (jobs) => {
    const normalizedData = normalizeData({jobs});

    return {
        jobs: normalizedData.jobs || {},
        jobPositions: normalizedData.jobPositions || {}
    };
};

export const data = {
    roles: normalizeRoleList(roles),
    employees: normalizeEmployeeList(employees),
    jobs: normalizeJobList(jobs).jobs,
    jobPositions: normalizeJobList(jobs).jobPositions,
};
