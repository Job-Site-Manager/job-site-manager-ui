import {normalizeRoleList, normalizeEmployeeList, normalizeJobList} from './normalizr';
import roles from './test-data/roles';
import jobs from "./test-data/jobs";
import employees from "./test-data/employees";


describe('Normalizr', () => {

    it('normalizeRoleList', () => {
        const expectedNormalizedRoles = {
            '1': {
                id: 1,
                    name: 'Bulldozer Operator',
                    notes: 'Requires a bulldozer'
            },
            '2': {
                id: 2,
                    name: 'Backhoe Operator',
                    notes: 'Requires a backhoe'
            },
            '3': {
                id: 3,
                    name: 'Rock Truck Operator',
                    notes: null
            },
            '4': {
                id: 4,
                    name: 'Scraper Operator',
                    notes: 'Driver should not have a sore back'
            },
            '5': {
                id: 5,
                    name: 'Packer Operator',
                    notes: 'Requires a packer'
            }
        };

        const normalizedRoles = normalizeRoleList(roles);

        expect(normalizedRoles).toEqual(expectedNormalizedRoles);
    });

    it('normalizeEmployeeList', () => {
        const expectedNormalizedEmployees = {
            '1': {
                id: 1,
                name: 'Tom Houston',
                address: '12 Steep Road',
                phoneNumber: '519-425-6434',
                primaryRoles: [
                    1
                ],
                secondaryRoles: [
                    2,
                    4
                ]
            },
            '2': {
                id: 2,
                name: 'Bill Crosby',
                address: '23 Rocky Road',
                phoneNumber: '519-734-9921',
                primaryRoles: [
                    2
                ],
                secondaryRoles: [
                    3,
                    4,
                    5
                ]
            },
            '3': {
                id: 3,
                name: 'Wayne Gretzky',
                address: '99 Greztky Lane',
                phoneNumber: '519-528-1884',
                primaryRoles: [
                    4
                ],
                secondaryRoles: []
            },
            '4': {
                id: 4,
                name: 'Tim Jefferson',
                address: '12 Smoke Cresent',
                phoneNumber: '519-625-6434',
                primaryRoles: [
                    3
                ],
                secondaryRoles: [
                    2
                ]
            },
            '5': {
                id: 5,
                name: 'Susan Moore',
                address: '1 First Road',
                phoneNumber: '519-625-6774',
                primaryRoles: [
                    4
                ],
                secondaryRoles: [
                    3,
                    2
                ]
            },
            '6': {
                id: 6,
                name: 'Auston Matthews',
                address: '7843 Dundas Steet',
                phoneNumber: '519-725-6494',
                primaryRoles: [
                    5
                ],
                secondaryRoles: [
                    2
                ]
            },
            '7': {
                id: 7,
                name: 'Mitch Marner',
                address: '12 Steep Road',
                phoneNumber: '519-625-6434',
                primaryRoles: [
                    5
                ],
                secondaryRoles: [
                    2
                ]
            },
            '8': {
                id: 8,
                name: 'Amber Rosen',
                address: '12 Half Road',
                phoneNumber: '519-929-6484',
                primaryRoles: [
                    1
                ],
                secondaryRoles: [
                    4,
                    5
                ]
            },
            '9': {
                id: 9,
                name: 'Jolene Rosen',
                address: '12 Half Road',
                phoneNumber: '519-929-6484',
                primaryRoles: [
                    1
                ],
                secondaryRoles: [
                    4,
                    5
                ]
            },
            '10': {
                id: 10,
                name: 'Ben Lammers',
                address: '12 Half Road',
                phoneNumber: '519-929-6484',
                primaryRoles: [
                    1
                ],
                secondaryRoles: [
                    4,
                    5
                ]
            }
        };

        const normalizedEmployees = normalizeEmployeeList(employees);

        expect(normalizedEmployees).toEqual(expectedNormalizedEmployees);
    });

    it('normalizeJobList', () => {
        const expectedNormalizedJobs = {
            jobs: {
                '1': {
                    id: 1,
                        name: 'The Old Pond',
                        address: '23 Pond Rd.',
                        jobPositions: [
                        1,
                        2,
                        3
                    ]
                },
                '2': {
                    id: 2,
                        name: 'Gravel Pit',
                        address: '434 Beach Rd.',
                        jobPositions: [
                        4,
                        5,
                        6
                    ]
                }
            },
            jobPositions: {
                '1': {
                    id: 1,
                        role: 3,
                        idealNumberOfEmployees: 2,
                        employees: [
                        5,
                        4
                    ]
                },
                '2': {
                    id: 2,
                        role: 2,
                        idealNumberOfEmployees: 1,
                        employees: [
                        2
                    ]
                },
                '3': {
                    id: 3,
                        role: 1,
                        idealNumberOfEmployees: 2,
                        employees: [
                        1
                    ]
                },
                '4': {
                    id: 4,
                        role: 5,
                        idealNumberOfEmployees: 2,
                        employees: [
                        6,
                        7
                    ]
                },
                '5': {
                    id: 5,
                        role: 4,
                        idealNumberOfEmployees: 1,
                        employees: [
                        3
                    ]
                },
                '6': {
                    id: 6,
                        role: 1,
                        idealNumberOfEmployees: 7,
                        employees: [
                        6,
                        8,
                        9,
                        10
                    ]
                }
            }
        };

        const normalizedJobs = normalizeJobList(jobs);

        expect(normalizedJobs).toEqual(expectedNormalizedJobs);
    });

});