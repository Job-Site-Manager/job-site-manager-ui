import * as actionCreators from './actionCreators';
import * as actions from './actions';

describe('Role Action Creators', () => {
    const role = {
        id: 1,
        name: 'Labourer',
        notes: 'Needs to be in good shape'
    };

    it('deleteRole', () => {
        const id = 1;

        const expectedAction = {
            type: actions.DELETE_ROLE,
            id
        };

        expect(actionCreators.createDeleteRoleAction(id)).toEqual(expectedAction);
    });

    it('updateRole', () => {
        const expectedAction = {
            type: actions.UPDATE_ROLE,
            role
        };

        expect(actionCreators.createUpdateRoleAction(role)).toEqual(expectedAction);
    });
});