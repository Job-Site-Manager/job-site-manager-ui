import uuid from "uuid";
import * as serverCalls from "../../../../server-calls";
import * as roleActions from './actions';
import reducer from "../../reducers";

export const resetRoles = roles => ({
    type: roleActions.RESET_ROLES,
    roles,
});

export const addPlaceholderRole = role => ({
    type: roleActions.ADD_PLACEHOLDER_ROLE,
    role: { ...role, placeholder: true }
});

const addRoleFromServer = (role, placeholderId) => ({
    type: roleActions.ADD_ROLE,
    placeholderId,
    role
});

export const addRole = role => {
    const placeholderId = uuid.v4();

    return (dispatch) => {
        const success = newRole => dispatch(addRoleFromServer(newRole, placeholderId));

        const error = () => console.log("ERROR: FAILED TO ADD ROLE");

        dispatch(addPlaceholderRole({...role, id: placeholderId}));

        serverCalls.postRole(role, success, error);
    };
};

export const createDeleteRoleAction = id => ({
    type: roleActions.DELETE_ROLE,
    id
});

export const deleteRoleFailed = role => ({
    type: roleActions.DELETE_ROLE_FAILED,
    role
});

const removeEmployee = (dispatch, state, id) => {
    const roleToDelete = reducer.roles.getRole(state, id);

    const error = () => {
        dispatch(deleteRoleFailed(roleToDelete));
        console.log("ERROR: FAILED TO DELETE ROLE");
    };

    dispatch(createDeleteRoleAction(id));

    serverCalls.deleteRole(id, error);
};

const generateRoleAssignedErrorMessage = (jobNames, employeeNames) => {
    let assignedMessage = 'ERROR: Role cannot be removed (Roles is assigned to the following ';
    if(jobNames.length > 0) {
        assignedMessage += `jobs: ${jobNames.join(', ')}`;

        if(employeeNames.length > 0) {
            assignedMessage += ' and ';
        }
    }

    if(employeeNames.length > 0) {
        assignedMessage += `employees: ${employeeNames.join(', ')}`;
    }

    assignedMessage += ')';

    return assignedMessage;
};

const logRoleAssignedError = (state, roleId) => {
    const jobNames = reducer.jobs.getJobsRoleIsAssignedTo(state, roleId).map(job => job.name);
    const employeeNames = reducer.employees.getEmployeesRoleIsAssignedTo(state, roleId).map(employee => employee.name);

    console.log(generateRoleAssignedErrorMessage(jobNames, employeeNames));
};

const isRoleAssignedToJobOrEmployee = (state, roleId) => {
    return (
        reducer.jobs.isRoleAssignedToAtLeastOneJob(state, roleId) ||
        reducer.employees.isRoleAssignedToAtLeastOneEmployee(state, roleId)
    );
};


export const deleteRole = id => {
    return (dispatch, getState) => {
        const state = getState();
        if(isRoleAssignedToJobOrEmployee(state, id)) {
            logRoleAssignedError(state, id);
        } else {
            removeEmployee(dispatch, state, id);
        }
    };
};

export const createUpdateRoleAction = role => ({
    type: roleActions.UPDATE_ROLE,
    role
});

const updateRoleFailed = role => ({
    type: roleActions.UPDATE_ROLE_FAILED,
    role
});

export const updateRole = updatedRole => {
    return (dispatch, getState) => {
        const roleToUpdate = reducer.roles.getRole(getState(), updatedRole.id);

        const error = () => {
            dispatch(updateRoleFailed(roleToUpdate));
            console.log("ERROR: FAILED TO UPDATE ROLE");
        };

        dispatch(createUpdateRoleAction(updatedRole));

        serverCalls.updateRole(updatedRole, error);
    };
};