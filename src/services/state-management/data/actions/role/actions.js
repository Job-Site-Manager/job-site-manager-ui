export const RESET_ROLES = 'RESET_ROLES';
export const ADD_ROLE = 'ADD_ROLE';
export const ADD_PLACEHOLDER_ROLE = 'ADD_PLACEHOLDER_ROLE';
export const DELETE_ROLE = 'DELETE_ROLE';
export const DELETE_ROLE_FAILED = 'DELETE_ROLE_FAILED';
export const UPDATE_ROLE = 'UPDATE_ROLE';
export const UPDATE_ROLE_FAILED = 'UPDATE_ROLE_FAILED';


export const isTypeResetRoles = action => action.type === RESET_ROLES;

export const isTypeAddRole = action => action.type === ADD_ROLE;

export const isTypeAddPlaceholderRole = action => action.type === ADD_PLACEHOLDER_ROLE;

export const isTypeDeleteRole = action => action.type === DELETE_ROLE;

export const isTypeDeleteRoleFailed = action => action.type === DELETE_ROLE_FAILED;

export const isTypeUpdateRole = action => action.type === UPDATE_ROLE;

export const isTypeUpdateRoleFailed = action => action.type === UPDATE_ROLE_FAILED;