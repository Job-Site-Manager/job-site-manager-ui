import {initialDataReceived} from "../../state/action/loading";
import {resetJobs} from "./job/actionCreators";
import {resetEmployees} from "./employee/actionCreators";
import {normalizeEmployeeList, normalizeJobList, normalizeRoleList} from "../service/normalizr";
import {getAllData} from "../../../server-calls";
import {resetRoles} from "./role/actionCreators";

export const fetchData = () => {
    return (dispatch) => {
        const success = data => {
            dispatch(resetRoles(normalizeRoleList(data.roles)));
            dispatch(resetEmployees(normalizeEmployeeList(data.employees)));
            dispatch(resetJobs(normalizeJobList(data.jobs)));
            dispatch(initialDataReceived());
        };

        const error = () => console.log("ERROR: FAILED TO FETCH DATA");

        getAllData(success, error);
    };
};