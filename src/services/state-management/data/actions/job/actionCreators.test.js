import * as actionCreators from "./actionCreators";
import * as actions from './actions';

describe('Job Action Creators', () => {
    const job = {
        id: 3,
        name: "The New Job",
        address: "1 Pond Rd.",
        jobPositions: [
            {
                id: 4,
                role: 2,
                idealNumberOfEmployees: 2,
                employees: [
                    5,
                    4
                ]
            },
            {
                id: 5,
                role: 3,
                idealNumberOfEmployees: 2,
                employees: [
                    6,
                    7
                ]
            },
        ]
    };

    it('createDeleteJobAction', () => {
        const id = 1;

        const expectedAction = {
            type: actions.DELETE_JOB,
            id
        };

        expect(actionCreators.createDeleteJobAction(id)).toEqual(expectedAction);
    });
});