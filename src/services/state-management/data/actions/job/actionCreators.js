import {
    ADD_JOB,
    ADD_PLACEHOLDER_JOB,
    DELETE_JOB,
    RESET_JOBS,
    UPDATE_JOB,
    DELETE_JOB_FAILED,
    UPDATE_JOB_FAILED
} from "./actions";
import uuid from "uuid";
import * as serverCalls from "../../../../server-calls";
import reducer from "../../reducers";

export const resetJobs = jobs => ({
    type: RESET_JOBS,
    jobs,
});

export const addPlaceholderJob = job => {
    const placeholderJobPositions = job.jobPositions.map(jobPosition => ({...jobPosition, placeholder: true}));

    return {
        type: ADD_PLACEHOLDER_JOB,
        job: {...job, jobPositions: placeholderJobPositions, placeholder: true}
    };
};

const addJobFromServer = (job, placeholderId) => ({
    type: ADD_JOB,
    job,
    placeholderId
});

export const addJob = job => {
    const placeholderId = uuid.v4();

    return (dispatch) => {
        const success = newJob => dispatch(addJobFromServer(newJob, placeholderId));

        const error = () => console.log("ERROR: FAILED TO ADD JOB");

        dispatch(addPlaceholderJob({...job, id: placeholderId }));

        serverCalls.postJob(job, success, error);
    };
};

export const createDeleteJobAction = id => ({
    type: DELETE_JOB,
    id
});

export const deleteJobFailed = job => ({
    type: DELETE_JOB_FAILED,
    job
});

export const deleteJob = id => {
    return (dispatch, getState) => {
        const jobToDelete = reducer.jobs.getJob(getState(), id);

        const error = () => {
            dispatch(deleteJobFailed(jobToDelete));
            console.log("ERROR: FAILED TO DELETE JOB");
        };

        dispatch(createDeleteJobAction(id));

        serverCalls.deleteJob(id, error);
    };
};

export const createUpdateJobAction = job => ({
    type: UPDATE_JOB,
    job
});

const updateJobFailed = job => ({
    type: UPDATE_JOB_FAILED,
    job
});

export const updateJob = updatedJob => {
    return (dispatch, getState) => {
        const jobToUpdate = reducer.jobs.getJob(getState(), updatedJob.id);

        const error = () => {
            dispatch(updateJobFailed(jobToUpdate));
            console.log("ERROR: FAILED TO UPDATE JOB");
        };

        dispatch(createUpdateJobAction(updatedJob));

        serverCalls.updateJob(updatedJob, error);
    };
};