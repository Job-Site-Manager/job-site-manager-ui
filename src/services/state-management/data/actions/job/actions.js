export const RESET_JOBS = 'RESET_JOBS';
export const ADD_JOB = 'ADD_JOB';
export const ADD_PLACEHOLDER_JOB = 'ADD_PLACEHOLDER_JOB';
export const DELETE_JOB = 'DELETE_JOB';
export const UPDATE_JOB = 'UPDATE_JOB';
export const DELETE_JOB_FAILED = 'DELETE_JOB_FAILED';
export const UPDATE_JOB_FAILED = 'UPDATE_JOB_FAILED';

export const isTypeResetJobs = action => action.type === RESET_JOBS;

export const isTypeAddJob = action => action.type === ADD_JOB;

export const isTypeAddPlaceholderJob = action => action.type === ADD_PLACEHOLDER_JOB;

export const isTypeDeleteJob = action => action.type === DELETE_JOB;

export const isTypeUpdateJob = action => action.type === UPDATE_JOB;

export const isTypeDeleteJobFailed = action => action.type === DELETE_JOB_FAILED;

export const isTypeUpdateJobFailed = action => action.type === UPDATE_JOB_FAILED;