import * as actionCreators from "./actionCreators";
import * as actions from './actions';

describe('Employee Action Creators', () => {
    const employee = {
        "id": 10,
        "name": "Ben Lammers",
        "address": "12 Half Road",
        "phoneNumber": "519-929-6484",
        "primaryRoles": [
            1
        ],
        "secondaryRoles": [
            4,
            5
        ]
    };

    it('deleteEmployee', () => {
        const id = 1;

        const expectedAction = {
            type: actions.DELETE_EMPLOYEE,
            id
        };

        expect(actionCreators.createDeleteEmployeeAction(id)).toEqual(expectedAction);
    });

    it('updateEmployee', () => {
        const expectedAction = {
            type: actions.UPDATE_EMPLOYEE,
            employee
        };

        expect(actionCreators.createUpdateEmployeeAction(employee)).toEqual(expectedAction);
    });
});