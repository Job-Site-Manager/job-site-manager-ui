export const RESET_EMPLOYEES = 'RESET_EMPLOYEES';
export const ADD_EMPLOYEE = 'ADD_EMPLOYEE';
export const ADD_PLACEHOLDER_EMPLOYEE = 'ADD_PLACEHOLDER_EMPLOYEE';
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const DELETE_EMPLOYEE_FAILED = 'DELETE_EMPLOYEE_FAILED';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
export const UPDATE_EMPLOYEE_FAILED = 'UPDATE_EMPLOYEE_FAILED';


export const isTypeResetEmployees = action => action.type === RESET_EMPLOYEES;

export const isTypeAddEmployee = action => action.type === ADD_EMPLOYEE;

export const isTypeAddPlaceholderEmployee = action => action.type === ADD_PLACEHOLDER_EMPLOYEE;

export const isTypeDeleteEmployee = action => action.type === DELETE_EMPLOYEE;

export const isTypeDeleteEmployeeFailed = action => action.type === DELETE_EMPLOYEE_FAILED;

export const isTypeUpdateEmployee = action => action.type === UPDATE_EMPLOYEE;

export const isTypeUpdateEmployeeFailed = action => action.type === UPDATE_EMPLOYEE_FAILED;