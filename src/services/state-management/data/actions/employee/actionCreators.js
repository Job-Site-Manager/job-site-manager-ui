import uuid from "uuid";
import {postEmployee} from "../../../../server-calls";
import * as actions from './actions';
import reducer from "../../reducers";
import * as serverCalls from "../../../../server-calls";

export const resetEmployees = employees => ({
    type: actions.RESET_EMPLOYEES,
    employees
});

export const addPlaceholderEmployee = employee => ({
    type: actions.ADD_PLACEHOLDER_EMPLOYEE,
    employee: {...employee, placeholder: true}
});

const addEmployeeFromServer = (employee, placeholderId) => ({
    type: actions.ADD_EMPLOYEE,
    placeholderId,
    employee
});

export const addEmployee = employee => {
    const placeholderId = uuid.v4();

    return (dispatch) => {
        const success = newRole => dispatch(addEmployeeFromServer(newRole, placeholderId));

        const error = () => console.log("ERROR: FAILED TO ADD EMPLOYEE");

        dispatch(addPlaceholderEmployee({...employee, id: placeholderId }));

        postEmployee(employee, success, error);
    };
};

export const createDeleteEmployeeAction = id => ({
    type: actions.DELETE_EMPLOYEE,
    id
});

const deleteEmployeeFailed = employee => ({
    type: actions.DELETE_EMPLOYEE_FAILED,
    employee
});

const removeEmployee = (dispatch, state, id) => {
    const employeeToDelete = reducer.employees.getEmployee(state, id);

    const error = () => {
        dispatch(deleteEmployeeFailed(employeeToDelete));
        console.log("ERROR: FAILED TO DELETE EMPLOYEE");
    };

    dispatch(createDeleteEmployeeAction(id));

    serverCalls.deleteEmployee(id, error);
};

const logEmployeeAssignedError = (state, id) => {
    const jobs = reducer.jobs.getJobsEmployeeIsAssignedTo(state, id);
    const jobNames = jobs.map(job => job.name);
    console.log(
        `ERROR: Employee cannot be removed (Employee is assigned to the following jobs: ${jobNames.join(', ')})`
    )
};

export const deleteEmployee = id => {
    return (dispatch, getState) => {
        const state = getState();
        if(reducer.jobs.isEmployeeAssignedToAtLeastOneJob(state, id)) {
            logEmployeeAssignedError(state, id);
        } else {
            removeEmployee(dispatch, state, id);
        }
    };
};

export const createUpdateEmployeeAction = employee => ({
    type: actions.UPDATE_EMPLOYEE,
    employee
});

const updateEmployeeFailed = employee => ({
    type: actions.UPDATE_EMPLOYEE_FAILED,
    employee
});

export const updateEmployee = updatedEmployee => {
    return (dispatch, getState) => {
        const employeeToUpdate = reducer.employees.getEmployee(getState(), updatedEmployee.id);

        const error = () => {
            dispatch(updateEmployeeFailed(employeeToUpdate));
            console.log("ERROR: FAILED TO UPDATE EMPLOYEE");
        };

        dispatch(createUpdateEmployeeAction(updatedEmployee));

        serverCalls.updateEmployee(updatedEmployee, error);
    };
};