export const INITIAL_DATA_RECEIVED = 'INITIAL_DATA_RECEIVED';

export const initialDataReceived = () => ({
    type: INITIAL_DATA_RECEIVED,
});