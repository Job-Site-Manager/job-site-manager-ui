import {INITIAL_DATA_RECEIVED} from '../../action/loading';

const loading = (state = true, action) => {
    if(action.type === INITIAL_DATA_RECEIVED) {
        return false;
    }

    return state;
};

export default loading;