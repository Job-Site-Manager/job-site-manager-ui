import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import { combineReducers } from 'redux';
import thunk from 'redux-thunk';
import dataReducer from './data/reducers/index';
import loadingReducer from './state/reducer/loading';

const reducer = combineReducers({
    data: dataReducer.reducer,
    loading: loadingReducer
});

const store = createStore(
    reducer,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

export default store;