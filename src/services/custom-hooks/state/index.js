import {useState} from "react";

export const useFormInputs = (initialValue) => {
    const [value, setValue] = useState(initialValue || '');

    const handleChange = event => setValue(event.target.value);

    return {
        value,
        onChange: handleChange
    }
};

export const useObjectArrayState = (initialState) => {
    const [objectArray, setObjectArray] = useState(initialState || []);

    const addObject = object => setObjectArray(objectArray.concat(object));

    const removeObject = id => setObjectArray(objectArray.filter(object => object.id !== id));

    return {
        value: objectArray,
        addObject,
        removeObject,
    }
};