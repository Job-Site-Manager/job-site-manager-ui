import React, { Component } from 'react';
import {connect} from "react-redux";
import {fetchData} from "./services/state-management/data/actions/data";
import PageLayout from "./components/PageLayout";

class App extends Component {
    render() {
        this.props.dispatch(fetchData());

        return <PageLayout loading={this.props.loading}/>
    }

}

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
    }
};

export default connect(
    mapStateToProps,
)(App);
