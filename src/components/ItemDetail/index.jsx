import React from "react";
import styled from 'styled-components';

const ItemDetailWrapper = styled.section`
    border-style: solid; 
    border-width: 1px;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.1);
    margin: 0 8px 8px;
    font-size: 14px;
`;

const Title = styled.h4`
    margin: 0;
    text-align: center;
    padding: 2px;
    border-bottom-style: solid; 
    border-bottom-width: 1px;
    font-weight: normal;
    background-color: #f6f6f6;
`;

const Content = styled.div`
    text-align: center;
    padding: 2px;
`;

const ItemDetail = ({name, value, children}) => {
    let body = children;

    if (!body) {
        body = (<Content>{value}</Content>);
    }

    return (
        <ItemDetailWrapper>
            <Title>{name}</Title>
            {body}
        </ItemDetailWrapper>
    );
};

export default ItemDetail;