import React, {useState} from 'react';
import styled from 'styled-components';
import AddButton from '../AddButton/index';

const ToggleableFormWrapper = styled.div`
    margin: 15px 0;
`;

const ToggleableForm = ({onFormSubmission, children}) => {
    const [formDisplayed, setFormDisplayed] = useState(false);

    const handleDisplayingForm = () => setFormDisplayed(true);

    const handleHidingForm = () => setFormDisplayed(false);

    const handleFormSubmission = (item) => {
        onFormSubmission(item);
        handleHidingForm();
    };

    const cloneChildFormPassingItRequiredProps = () => {
        return React.cloneElement(
            children,
            {
                submissionButtonName: "Create",
                onCancelClicked: handleHidingForm,
                onFormSubmission: handleFormSubmission,
            },
        )
    };

    const render = () => {
        let component;

        if(formDisplayed) {
            component = cloneChildFormPassingItRequiredProps();
        } else {
            component = <AddButton onButtonClick={handleDisplayingForm}/>;
        }

        return (
            <ToggleableFormWrapper>
                {component}
            </ToggleableFormWrapper>
        );
    };

    return render();
};

export default ToggleableForm;