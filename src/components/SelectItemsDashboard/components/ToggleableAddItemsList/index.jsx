import React, { useState } from 'react';
import CollapsableAddItemsList from "./components/CollapsableAddItemsList";
import AddButton from "../../../AddButton";

const ToggleableAddItemsList = ({items, addItem, notActive}) => {
    const [selected, setSelected] = useState(false);

    const handleOpeningAddItemsList = () => setSelected(true);

    const handleCollapsingAddItemsList = () => setSelected(false);

    const render = () => {
        if (!selected) {
            return (
                <AddButton small onButtonClick={handleOpeningAddItemsList} notActive={notActive}/>
            );
        } else {
            return (
                <CollapsableAddItemsList
                    items={items}
                    addItem={addItem}
                    onCollapseButtonClick={handleCollapsingAddItemsList}
                />
            );
        }
    };

    return render();
};

export default ToggleableAddItemsList;