import React from 'react';
import styled from "styled-components";
import SelectableItemsList from "../../../SelectableItemsList";
import AddItemButton from "../../../AddItemButton";

const CollapsibleAddItemsListWrapper = styled.div`
    border-style: solid; 
    border-width: 1px;
    margin: 0 8px;
`;

const SelectableItemsListContainer = styled.section`
    border-bottom-style: solid; 
    border-bottom-width: 1px;
    padding-top: 10px;
`;

const CollapsePanel = styled.div`
    display: flex;
    justify-content: center;
    padding: 2px;
`;

const CollapsibleAddItemsList = ({items, onCollapseButtonClick, addItem}) => (
    <CollapsibleAddItemsListWrapper>
        <SelectableItemsListContainer>
            <SelectableItemsList items={items} selectItem={addItem} button={<AddItemButton />}/>
        </SelectableItemsListContainer>
        <CollapsePanel>
            <button onClick={onCollapseButtonClick}>-</button>
        </CollapsePanel>
    </CollapsibleAddItemsListWrapper>
);

export default CollapsibleAddItemsList;
