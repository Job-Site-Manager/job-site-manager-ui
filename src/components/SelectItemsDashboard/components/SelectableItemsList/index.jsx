import React from 'react';
import styled from 'styled-components';
import uuid from "uuid";

const SelectableItemsListWrapper = styled.ul`
    margin: 0;
    padding: 0;
`;

const Item = styled.div`
    margin: 0 8px 8px;
    border-style: solid; 
    border-width: 1px;
    padding: 2px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .1);
`;

const SelectableItemsList = ({items, button, selectItem}) => {
    const itemComponents = items.map((item) => {
        const selectItemOnClick = () => {
            if(selectItem) {
                selectItem(item);
            }
        };

        return (
            <Item key={uuid.v4()} onClick={selectItemOnClick}>
                {item.name}
                {button}
            </Item>
        )
    });

    return (
        <SelectableItemsListWrapper>
            {itemComponents}
        </SelectableItemsListWrapper>
    )
};

export default SelectableItemsList;