import React from 'react';
import styled from 'styled-components';
import Button from "../Button";

const RemoveButtonWrapper = styled(Button)`
    color: #db2828;
    border-color: #db2828;
`;


const RemoveItemButton = () => (
    <RemoveButtonWrapper>
        -
    </RemoveButtonWrapper>
);

export default RemoveItemButton;