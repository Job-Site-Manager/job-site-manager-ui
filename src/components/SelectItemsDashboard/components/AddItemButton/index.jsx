import React from 'react';
import styled from 'styled-components';
import Button from "../Button";

const AddButtonWrapper = styled(Button)`
    padding-bottom: 3px;
    color: #559a28;
    border-color: #559a28;
`;


const AddItemButton = () => (
    <AddButtonWrapper>
        +
    </AddButtonWrapper>
);

export default AddItemButton;
