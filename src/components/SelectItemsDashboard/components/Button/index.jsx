import styled from "styled-components";

const Button = styled.button`
    position: absolute;
    right: 2px;
    height: 18px;
    padding: 0 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    border-style: solid; 
    border-width: 1px;
    font-size: 20px;
    line-height: 0;
`;

export default Button;