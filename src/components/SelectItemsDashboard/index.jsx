import React from 'react';
import RemoveItemButton from './components/RemoveItemButton';
import SelectableItemsList from "./components/SelectableItemsList";
import ToggleableAddItemsList from "./components/ToggleableAddItemsList";
import ItemContentSection from "../ItemContentSection";

const SelectItemsDashboard = ({
    title,
    selectedItems,
    availableItems,
    maxNumberOfItems,
    addItem,
    removeItem,
    className
}) => {
    const emptySlots = [];
    let notActive = false;
    if(maxNumberOfItems) {
        for(let i = selectedItems.length; i < maxNumberOfItems; i++) {
            emptySlots.push({
                name: "---  ---  ---",
            })
        }
        notActive = maxNumberOfItems <= selectedItems.length;
    }

    const body = (
        <div className={className}>
            <SelectableItemsList selectItem={removeItem} items={selectedItems} button={<RemoveItemButton/>}/>
            <SelectableItemsList items={emptySlots} button={null}/>
            <ToggleableAddItemsList items={availableItems} addItem={addItem} notActive={notActive}/>
        </div>
    );

    if(title) {
        return (
            <ItemContentSection title={title}>
                {body}
            </ItemContentSection>
        );
    } else {
        return (body);
    }
};

export default SelectItemsDashboard;