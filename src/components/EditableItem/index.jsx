import React, { useState } from 'react';
import Item from "./components/Item";

const EditableItem = ({name, id, itemBody, form, onUpdate, onRemoveItem}) => {
    const [editFormOpen, setEditFormOpen] = useState(false);

    const handleItemUpdate= (updatedItem, identifier) => {
        onUpdate(updatedItem, identifier);
        closeForm();
    };

    const handleEditClick = () => openForm();

    const handleFormClose = () => closeForm();

    const openForm = () => setEditFormOpen(true);

    const closeForm = () => setEditFormOpen(false);

    const renderFormPropPassingItRequiredProps = () => {
        return React.cloneElement(
            form,
            {
                onCancelClicked: handleFormClose,
                onFormSubmission: handleItemUpdate,
            },
        )
    };

    const handleRemoveItem = () => onRemoveItem(id);

    const renderItem = () => {
        return (
            <Item
                name={name}
                onEditClick={handleEditClick}
                onRemoveItem={handleRemoveItem}
            >
                {itemBody}
            </Item>
        );
    };

    const render = () => {
        if (editFormOpen) {
            return renderFormPropPassingItRequiredProps();
        } else {
            return renderItem();
        }
    };

    return render();
};

export default EditableItem;