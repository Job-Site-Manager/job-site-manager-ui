import React, { useState } from 'react';
import styled from 'styled-components';
import ItemHeader from "./components/ItemHeader/index";

const ItemWrapper = styled.section`
    margin-bottom: 8px;
    border-style: solid; 
    border-width: 1px; 
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .1);
    
    ${props => {
        if(!props.collapsed) {
            return "box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .3);";
        }
    }}
`;

const Content = styled.div`
    border-bottom-style: solid; border-bottom-width: 1px;
    padding: 5px;
`;

const Item = ({name, onRemoveItem, onEditClick, children}) => {
    const [collapsed, setCollapsed] = useState(true);

    const handleTogglingContent = () => setCollapsed(!collapsed);

    const conditionallyRenderContent = () => {
        if(!collapsed) {
            return (
                <Content>
                    {children}
                </Content>
            );
        }
    };

    const render = () => {
        return (
            <ItemWrapper collapsed={collapsed}>
                <ItemHeader
                    onHeaderClicked={handleTogglingContent}
                    name={name}
                    onEditClick={onEditClick}
                    onRemoveClick={onRemoveItem}
                />
                {conditionallyRenderContent()}
            </ItemWrapper>
        );
    };

    return render();
};

export default Item;