import React from 'react';
import styled from 'styled-components';
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ItemHeaderWrapper = styled.header`
    padding: 5px 60px;
    border-bottom-style: solid; 
    border-bottom-width: 1px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
`;

const Title = styled.h2`
    margin: 0;
    font-size: 18px;
    font-weight: bold;
`;

const Icons = styled.span`
    position: absolute;
    right: 5px;
    display: flex;
    align-items: center;
`;

const Remove = styled.button`
    margin: 0;
    margin-left: 5px;
    height: 20px;
    padding-bottom: 5px;
    width: 20px;
    border-style: solid; 
    border-width: 1px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 30px;
    background-color: #eee;
    line-height: 0;
`;

const ItemHeader = ({name,onHeaderClicked, onRemoveClick, onEditClick}) => {

    return (
        <ItemHeaderWrapper onClick={onHeaderClicked}>
            <Title>{name}</Title>
            <Icons>
                <FontAwesomeIcon icon={faEdit} onClick={onEditClick}/>
                <Remove onClick={onRemoveClick}>-</Remove>
            </Icons>
        </ItemHeaderWrapper>
    );
};

export default ItemHeader;