import React from "react";
import styled from 'styled-components';

const TextAreaFormEntryWrapper = styled.section`
    padding-bottom: 10px;
    display: flex;
    flex-direction: column;
`;

const TextArea = styled.textarea`
    font-size: 16px;
    resize: none;
`;

const TextAreaFormEntry = ({name, value, onTextAreaChange}) => (
    <TextAreaFormEntryWrapper>
        <label>{name}</label>
        <TextArea
            onChange={onTextAreaChange}
            value={value}
            cols="40"
            rows="5"
        />
    </TextAreaFormEntryWrapper>
);

export default TextAreaFormEntry;