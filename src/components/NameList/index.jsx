import React from "react";
import styled from 'styled-components';

const NameListWrapper = styled.ul`
    margin: 0;
    padding: 0;
`;

const Name = styled.div`
    margin: 0 8px 8px;
    border-style: solid; 
    border-width: 1px;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .1);
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-size: 14px;
    padding: 2px;
`;

const NameList = ({names}) => {
    let roleNameComponents = names.map(name => (
        <Name key={name}>{name}</Name>
    ));

    return (
        <NameListWrapper>
            {roleNameComponents}
        </NameListWrapper>
    );
};

export default NameList;