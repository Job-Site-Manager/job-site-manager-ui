import React from "react";
import styled from 'styled-components';

const ItemContentSectionWrapper = styled.section`
    margin-bottom: 8px;
`;

const Title = styled.h3`
    margin: 0 0 10px;
    padding: 5px 0;
    border-bottom-style: solid; border-bottom-width: 1px;
    display: flex;
    justify-content: ${props => props.centerTitle? 'center' : 'left'  };
    font-size: 16px;
    font-weight: bold;
`;

const ItemContentSection = ({title, centerTitle, children}) => (
    <ItemContentSectionWrapper>
        <Title centerTitle={centerTitle}>{title}</Title>
        {children}
    </ItemContentSectionWrapper>
);

export default ItemContentSection;