import React from "react";
import styled from 'styled-components';

const InputFormEntryWrapper = styled.section`
    padding-bottom: 10px;
    display: flex;
    flex-direction: column;
`;

const Input = styled.input`
    height: 28px;
    font-size: 16px;
    border-style: solid; 
    border-width: 1px;
`;

const InputFormEntry = ({name, value, onInputChange}) => (
    <InputFormEntryWrapper>
        <label>{name}</label>
        <Input
            type="text"
            value={value}
            onChange={onInputChange}
        />
    </InputFormEntryWrapper>
);

export default InputFormEntry;