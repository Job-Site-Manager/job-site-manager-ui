import React from 'react';
import styled from 'styled-components';

// TODO: this isn't the best way to do this
const AddButtonWrapper = styled.button`
    margin: auto;
    margin-bottom: 5px;
    height: ${props => props.small ? "30px" : "44px"};
    width: ${props => props.small ? "30px" : "44px"};
    padding: 0;
    padding-bottom: ${props => props.small ? "6px" : "8px"};
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    border-style: solid; 
    border-width: 1px;
    font-size: ${props => props.small ? "34px" : "50px"};
    line-height: 0;
    opacity: ${props => props.notActive ? "0.5" : "1"};
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .1);
    :focus {outline:0;}
`;

const AddButton = ({small, onButtonClick, notActive}) => {
    let handleButtonClick = onButtonClick;

    if(notActive) {
        handleButtonClick = event => event.preventDefault();
    }

    return (
        <AddButtonWrapper notActive={notActive} small={small} onClick={handleButtonClick}>
            +
        </AddButtonWrapper>
    );
};

export default AddButton;