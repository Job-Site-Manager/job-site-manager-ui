import React from 'react';
import styled from 'styled-components';

const Title = styled.h1`
    margin: 0 0 30px;
    padding: 8px;
    border-bottom-style: solid; border-bottom-width: 1px;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-size: 30px;
    font-weight: bold;
`;

const Content = styled.div`
    max-width: 800px;
    margin-right: auto;
    margin-left: auto;
`;

const Dashboard = ({name, itemList, toggleableForm}) => (
    <section>
        <Title>
            {name}
        </Title>
        <Content>
            {itemList}
            {toggleableForm}
        </Content>
    </section>
);

export default Dashboard;