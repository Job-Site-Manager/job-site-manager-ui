import React from 'react';
import styled from 'styled-components';
import FormButtons from "./components/FormButtons";

const FormWrapper = styled.form`
    margin: 0 0 8px;
    padding: 5px;   
    display: flex;
    flex-direction: column;
    font-weight: bold;
    border-style: solid; 
    border-width: 1px;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, .1);
`;

const Form = ({submissionButtonName, onCancelClicked, onFormSubmission, children}) => (
    <FormWrapper>
        {children}
        <FormButtons
            submissionButtonName={submissionButtonName}
            onCancelClicked={onCancelClicked}
            onFormSubmission={onFormSubmission}
        />
    </FormWrapper>
);

export default Form;