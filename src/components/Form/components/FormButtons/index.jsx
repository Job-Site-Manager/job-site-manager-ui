import React from 'react';
import styled from 'styled-components';

const FormButtonsWrapper = styled.section`
    display: flex;
`;

const Button = styled.button`
    margin: 0;
    padding: 5px;
    flex-grow: 1;
    font-size: 16px;
    border: solid;
    border-width: 1px;
    border-radius: 0px;
`;

const SubmissionButton = styled(Button)`
    border-right: none;
    color: #2185d0;
    border-color: #2185d0;
`;

const CancelButton = styled(Button)`
    color: #db2828;
    border-color: #db2828;
`;

const FormButtons = ({submissionButtonName, onCancelClicked, onFormSubmission}) => {
    const handleCancelClicked = (event) => {
        event.preventDefault();
        onCancelClicked();
    };

    const handleFormSubmission = (event) => {
        event.preventDefault();
        onFormSubmission();
    };

    return (
        <FormButtonsWrapper>
            <SubmissionButton onClick={handleFormSubmission}>
                {submissionButtonName}
            </SubmissionButton>
            <CancelButton onClick={handleCancelClicked}>Cancel</CancelButton>
        </FormButtonsWrapper>
    );
};

export default FormButtons;