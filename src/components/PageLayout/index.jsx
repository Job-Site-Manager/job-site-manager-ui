import React, {useState} from "react";
import RolesDashboard from "../../scenes/RolesDashboard";
import EmployeesDashboard from "../../scenes/EmployeesDashboard";
import JobsDashboard from '../../scenes/JobsDashboard';
import styled from "styled-components";
import PageButtons from "./components/PageButtons";

export const pages = {
    roles: "Roles",
    employees: "Employees",
    jobs: "Jobs",
};

const PageLayoutWrapper = styled.div`
    margin-left: auto;
    margin-right: auto;
    max-width: 800px;
`;

const PageLayout = ({loading}) => {

    const [page, setPage] = useState(pages.jobs);

    const handleRolesButtonClick = () => setPage(pages.roles);

    const handleEmployeesButtonClick = () => setPage(pages.employees);

    const handleJobsButtonClick = () => setPage(pages.jobs);

    const getPageToDisplay = () => {
        if(loading) {
            return "LOADING (to be implemented)";
        }
        else if(page === pages.jobs) {
            return <JobsDashboard/>;
        }
        else if(page === pages.employees) {
            return <EmployeesDashboard/>;
        }
        else if(page === pages.roles) {
            return <RolesDashboard/>;
        }
    };

    const pageButtonDetails = [
        {
            handleClick: handleRolesButtonClick,
            text: pages.roles
        },
        {
            handleClick: handleEmployeesButtonClick,
            text: pages.employees
        },
        {
            handleClick: handleJobsButtonClick,
            text: pages.jobs
        },
    ];

    return (
        <PageLayoutWrapper>
            <PageButtons
                selectedPage={page}
                pageButtonDetails={pageButtonDetails}
            />
            {getPageToDisplay()}
        </PageLayoutWrapper>
    );
};

export default PageLayout;