import React from "react";
import styled from "styled-components";

const PageButtonsWrapper = styled.ul`
    margin: 0 0 20px;
    padding: 0;
    display: flex;
`;

const PageButton = styled.button`
    flex: 1 1 0;
    border-style: solid;
    border-width: 1px;
    padding: 12px;
    font-size: 16px;
    :focus {outline:0;}

    ${props => {
        if(props.selected) {
            return "color: #2185d0; border-bottom: 3px solid; font-weight: bold;";
        }
    }
}
`;

const PageButtons = ({pageButtonDetails, selectedPage}) => {
    const pageButtonComponents = pageButtonDetails.map((pageButtonDetail) => {
        const selected = selectedPage === pageButtonDetail.text;

        return (
            <PageButton
                key={pageButtonDetail.text}
                selected={selected}
                onClick={pageButtonDetail.handleClick}
            >
                {pageButtonDetail.text}
            </PageButton>
        );
    });

    return (
        <PageButtonsWrapper>
            {pageButtonComponents}
        </PageButtonsWrapper>
    );
};

export default PageButtons;