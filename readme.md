# Job Site Manager UI

Job Site Manager is a web application that helps companies who operate at multiple different job sites manage and track 
which employees are assigned to each site. Each employee in the system is assigned a set of roles that he/she can perform. 
When adding a new job site to the system you specifies the roles required for that site, and the number of employees need for 
each role. The application then helps you to fill the position with available, qualified employees. You can also update 
existing job sites as the demand for workers on the sites increase or decrease. 

This repository contains the UI for the Job Site Manager application. The Job Site Manager UI is a responsive web app built
using [React.js](https://reactjs.org/). 

Additional repositories for this application include:
- [Job Site Manager API](https://gitlab.com/Job-Site-Manager/job-site-manger-api)
- [Job Site Manager Deployment](https://gitlab.com/Job-Site-Manager/job-site-manager-deployment)

## Table of Contents
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
- [Running the Tests](#running-the-tests)
- [Folder Structure](#folder-structure)
- [Branching](#branching)
- [Built With](#built-with)
- [Deployment](#deployment)
- [Versioning](#versioning)
- [Authors](#authors)
- [Acknowledgements](#acknowledgments)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
The following will need to be installed on your local environment:
- [Node.js](https://nodejs.org/en/) 
- [npm](https://www.npmjs.com/)
- [Docker](https://www.docker.com/) (optional for development, required if you need to build a deployable container)
- [Docker-Compose](https://docs.docker.com/compose/) (optional)

### Installing

You have two options for running the application for local development. Both are very straightforward. If you are a 
fan of docker then you might want to skip to option two.

##### Option One
Run the application on your local computer. 

Steps:
1. Install the dependencies  
    a. In the terminal navigate to the root folder of the project    
    b. Run *npm install*  
2. Start the mock api server    
    a. In the terminal navigate to the folder */server*  
    b. Run *node index.js*
3. Start the react app  
    a. In the terminal move back to the root folder  
    b. Run *npm start*   

You should now have the mock api server running on *localhost:8080* and the react app should be accessible in your browser
at *localhost:3000*. The react app has hot reload set up so you can make changes in the 
code and you will see the changes automatically reflected in the browser.

##### Option Two
Run the application inside docker containers with the help of docker-compose. Note that this option requires that you 
installed the two optional dependencies docker and docker-compose (mentioned in the prerequisites section).

Steps:
1. Run the app with docker compose  
    a. In the terminal navigate to the root folder of the project  
    b. Run *docker-compose up*  
2. That's it. Docker and docker compose make our life that easy. The docker compose file started two docker containers one 
that is running the mock api server and one that serves the react app. 
 
You should now be able to access the mock api server running on *localhost:8080* and the react app should be accessible in your browser
at *localhost:3000*. The react app has hot reload set up so you can make changes in the 
code and you will see the changes automatically reflected in the browser.

## Running the tests
The unit tests use [Jest](https://jestjs.io/).

To execute the tests run: *npm test*

Optionally, you can install a Jest plugin for your IDE of choice that lets you to run the tests through the IDE. 

## Folder Structure

The basic structure is:
```
├── node_modules
├── public            
├── server                      // contains the mock api server used during development
├── src
│   ├── components              // components stored in this folder can be used anywhere in the application
│   │     ├─ List 
│   │     └─ Button
│   │         ├─ components     // components found in nested component folders can only be used by direct parents
│   │         └─ index.js
│   │
│   ├── scenes                  // scenes are pages in the application, 
│   │     │                     // anything defined within this scence folder can only be used in this scene
│   │     ├─ EmployeeDashboard     
│   │     └─ JobDashboard
│   │         ├─ components
│   │         └─ index.js
│   │ 
│   └── services                // services are self contain modules that contain business logic for the appication
│         ├─ List
│         └─ Button
│             ├─ components
│             └─ index.js
│ 
└── package.json
```

The motivation for the structure of the src file is based on the medium article [How to better organize your React applications?](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1) 
Before adding any folders to this application you should give the article a quick read through to better understand the structure and the motivation behind it.

## Branching 
This project uses the [gitflow branching](https://medium.com/@muneebsajjad/git-flow-explained-quick-and-simple-7a753313572f) pattern. 

When creating a personal branch off of a feature branch the following name pattern should be used:
*{developers-name}/{name-of-feature-branched-off-of}/{branch-description}*. For example if I branched off of the 
feature branch *feature/adding-jobs-page* to do work on the jobs form an appropriate name for the branch would be 
*eric/adding-jobs-page/creating-job-form*.

## Built With
- [React](https://reactjs.org/)
- [Create React App](https://github.com/facebook/create-react-app)
- [Redux](https://redux.js.org/)
- [Redux-Thunk](https://github.com/reduxjs/redux-thunk)
- [React-Redux](https://github.com/reduxjs/react-redux)
- [Styled-Components](https://www.styled-components.com/)
- [Normalizr](https://github.com/paularmstrong/normalizr)
- [Node](https://nodejs.org/en/)
- [Docker](https://www.docker.com/)
- [Jest](https://jestjs.io/)

## Deployment 
The application runs in a kubernetes cluster. Before a new version can be deployed to the cluster a docker container needs 
to be built and pushed to this projects [container registry](https://gitlab.com/Job-Site-Manager/job-site-manager-ui/container_registry). 

Steps:
1. Build the container  
    a. Run *docker build -t registry.gitlab.com/job-site-manager/job-site-manager-ui:{version} .*
2. Push the container to the register  
    a. Run *docker push registry.gitlab.com/job-site-manager/job-site-manager-ui:{version}*  
    b. Check the register [container registry](https://gitlab.com/Job-Site-Manager/job-site-manager-ui/container_registry) 
    to ensure that the container was correctly pushed
3. Use the [Job Site Manager Deployment](https://gitlab.com/Job-Site-Manager/job-site-manager-deployment) documentation to deploy the container
    
Notes:
- Always tag the container with the version number
- Make sure the tagged version number and the version in the package.json line up
- Might be a good idea to test the container locally before pushing to the registry 

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/Job-Site-Manager/job-site-manager-ui/tags). 
The [container registry](https://gitlab.com/Job-Site-Manager/job-site-manager-ui/container_registry) also contains containers for each 
version of the application.

## Authors
- Eric Lammers (ericjameslammers@gmail.com)

## Acknowledgments
- [Alexis Mangin](https://medium.com/@alexmngn) author of [How to better organize your React applications?](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1) 

