/* eslint-disable no-param-reassign */
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

let roles = require("./mock-data/roles").roles;
let employees = require('./mock-data/employees').employees;
let jobs = require('./mock-data/jobs').jobs;

const generalHelpers = require("./helpers/general");
const jobHelpers = require("./helpers/job");

const app = express();

app.set('port', (process.env.PORT || 8080));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Expires', '0');
    next();
});

app.get('/data', (req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.json({
        jobs,
        employees,
        roles,
    });
});

app.get('/roles', (req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.json(roles);
});

app.get('/employees', (req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.json(employees);
});

app.get('/jobs', (req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.json(jobs);
});

app.post('/role', (req, res) => {
    const role = req.body;

    const id = generalHelpers.getNextId(roles);
    const roleWithId = generalHelpers.addIdToItem(role, id);

    roles.push(roleWithId);

    res.status(201);
    res.json(roleWithId);
});

app.post('/employee', (req, res) => {
    const employee = req.body;

    const id = generalHelpers.getNextId(employees);
    const employeeWithId = generalHelpers.addIdToItem(employee, id);

    employees.push(employeeWithId);
    res.status(201);
    res.json(employeeWithId);
});

app.post('/job', (req, res) => {
    const job = req.body;

    const jobWithIds = jobHelpers.addIdsToJobAndJobPositions(jobs, job);

    jobs.push(jobWithIds);
    res.status(201);
    res.json(jobWithIds);
});

app.delete('/role/:id', (req, res) => {
    roles = generalHelpers.removeItemFromList(roles, generalHelpers.decodeString(req.params.id));

    res.status(200);
    res.send();
});

app.delete('/employee/:id', (req, res) => {
    employees = generalHelpers.removeItemFromList(employees, generalHelpers.decodeString(req.params.id));

    res.status(200);
    res.send();
});

app.delete('/job/:id', (req, res) => {
    jobs = generalHelpers.removeItemFromList(jobs, generalHelpers.decodeString(req.params.id));

    res.status(200);
    res.send();
});

app.put('/role/:id', (req, res) => {
    const updatedRole = req.body;

    roles = generalHelpers.createNewArrayWithItemUpdatedById(roles, updatedRole);

    res.status(204);
    res.send();
});

app.put('/employee/:id', (req, res) => {
    const updatedEmployee = req.body;

    employees = generalHelpers.createNewArrayWithItemUpdatedById(employees, updatedEmployee);

    res.status(204);
    res.send();
});

app.put('/job/:id', (req, res) => {
    const updatedJob = req.body;

    jobs = generalHelpers.createNewArrayWithItemUpdatedById(jobs, updatedJob);

    res.status(204);
    res.send();
});

app.listen(app.get('port'), () => {
    console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});
