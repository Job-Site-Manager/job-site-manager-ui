const generalHelpers = require("./general");

function getNextIdForJobPositions(jobs) {
    let largestId = 0;

    jobs.forEach(job => {
        const potentialId = generalHelpers.getNextId(job.jobPositions);

        if(potentialId > largestId) {
            largestId = potentialId;
        }
    });

    return largestId + 1;
}

function addIdsToJobAndJobPositions(jobs, job) {
    const jobId = generalHelpers.getNextId(jobs);
    let jobPositionId = getNextIdForJobPositions(jobs);

    job = generalHelpers.addIdToItem(job, jobId);

    const newJobPositions = [];
    job.jobPositions.forEach(jobPosition => {
        newJobPositions.push(generalHelpers.addIdToItem(jobPosition, jobPositionId));
        jobPositionId++;
    });

    job = Object.assign(job, {jobPositions: newJobPositions});

    return job;
}



exports.addIdsToJobAndJobPositions = addIdsToJobAndJobPositions;