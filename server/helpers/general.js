function decodeString(string) {
    return string.split('+').join(' ');
}

function removeItemFromList(list, id) {
    return list.filter(item => (item.id !== id && item.id !== parseInt(id)))
}

function getNextId(list) {
    let id = 0;

    list.forEach(item => {
        if(item.id > id) {
            id = item.id;
        }
    });

    return id + 1;
}

function addIdToItem(role, id) {
    return Object.assign({}, role, {id: id});
}

function createNewArrayWithItemUpdatedById(array, updatedItem) {
    return array.map((item) => {
        if(item.id === updatedItem.id) {
            return updatedItem;
        } else {
            return item;
        }
    });
}

exports.removeItemFromList = removeItemFromList;
exports.decodeString = decodeString;
exports.getNextId = getNextId;
exports.addIdToItem = addIdToItem;
exports.createNewArrayWithItemUpdatedById = createNewArrayWithItemUpdatedById;


