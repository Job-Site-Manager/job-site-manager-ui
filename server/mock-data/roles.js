const roles = [
    {
        "id": 1,
        "name": "Bulldozer Operator",
        "notes": "Requires a bulldozer"
    },
    {
        "id": 2,
        "name": "Backhoe Operator",
        "notes": "Requires a backhoe"
    },
    {
        "id": 3,
        "name": "Rock Truck Operator",
        "notes": null
    },
    {
        "id": 4,
        "name": "Scraper Operator",
        "notes": "Driver should not have a sore back"
    },
    {
        "id": 5,
        "name": "Packer Operator",
        "notes": "Requires a packer"
    }
];

exports.roles = roles;