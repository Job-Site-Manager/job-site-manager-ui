const employees = [
    {
        "id": 1,
        "name": "Tom Houston",
        "address": "12 Steep Road",
        "phoneNumber": "519-425-6434",
        "primaryRoles": [
            {
                "id": 1,
                "name": "Bulldozer Operator",
                "notes": "Requires a bulldozer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            },
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            }
        ]
    },
    {
        "id": 2,
        "name": "Bill Crosby",
        "address": "23 Rocky Road",
        "phoneNumber": "519-734-9921",
        "primaryRoles": [
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            }
        ],
        "secondaryRoles": [
            {
                "id": 3,
                "name": "Rock Truck Operator",
                "notes": null
            },
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            },
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ]
    },
    {
        "id": 3,
        "name": "Wayne Gretzky",
        "address": "99 Greztky Lane",
        "phoneNumber": "519-528-1884",
        "primaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            }
        ],
        "secondaryRoles": []
    },
    {
        "id": 4,
        "name": "Tim Jefferson",
        "address": "12 Smoke Cresent",
        "phoneNumber": "519-625-6434",
        "primaryRoles": [
            {
                "id": 3,
                "name": "Rock Truck Operator",
                "notes": null
            }
        ],
        "secondaryRoles": [
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            }
        ]
    },
    {
        "id": 5,
        "name": "Susan Moore",
        "address": "1 First Road",
        "phoneNumber": "519-625-6774",
        "primaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            }
        ],
        "secondaryRoles": [
            {
                "id": 3,
                "name": "Rock Truck Operator",
                "notes": null
            },
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            }
        ]
    },
    {
        "id": 6,
        "name": "Tom Rosen",
        "address": "12 Half Road",
        "phoneNumber": "519-929-6484",
        "primaryRoles": [
            {
                "id": 1,
                "name": "Bulldozer Operator",
                "notes": "Requires a bulldozer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            },
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ]
    },
    {
        "id": 6,
        "name": "Auston Matthews",
        "address": "7843 Dundas Steet",
        "phoneNumber": "519-725-6494",
        "primaryRoles": [
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            }
        ]
    },
    {
        "id": 7,
        "name": "Mitch Marner",
        "address": "12 Steep Road",
        "phoneNumber": "519-625-6434",
        "primaryRoles": [
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 2,
                "name": "Backhoe Operator",
                "notes": "Requires a backhoe"
            }
        ]
    },
    {
        "id": 8,
        "name": "Amber Rosen",
        "address": "12 Half Road",
        "phoneNumber": "519-929-6484",
        "primaryRoles": [
            {
                "id": 1,
                "name": "Bulldozer Operator",
                "notes": "Requires a bulldozer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            },
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ]
    },
    {
        "id": 9,
        "name": "Jolene Rosen",
        "address": "12 Half Road",
        "phoneNumber": "519-929-6484",
        "primaryRoles": [
            {
                "id": 1,
                "name": "Bulldozer Operator",
                "notes": "Requires a bulldozer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            },
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ]
    },
    {
        "id": 10,
        "name": "Ben Lammers",
        "address": "12 Half Road",
        "phoneNumber": "519-929-6484",
        "primaryRoles": [
            {
                "id": 1,
                "name": "Bulldozer Operator",
                "notes": "Requires a bulldozer"
            }
        ],
        "secondaryRoles": [
            {
                "id": 4,
                "name": "Scraper Operator",
                "notes": "Driver should not have a sore back"
            },
            {
                "id": 5,
                "name": "Packer Operator",
                "notes": "Requires a packer"
            }
        ]
    }
];

exports.employees = employees;